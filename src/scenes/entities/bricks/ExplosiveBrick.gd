extends Brick

@export var State: StateType

@onready var ExplosionLogic: Area2D = $ExplosionLogic
@onready var Animations: AnimationPlayer = $Animations

var has_exploded := false

func _on_brick_logic_effect(ball: Ball, type: E.BrickEffect):
	match type:
		E.BrickEffect.EXPLOSIVE:
			if State.show_points:
				Logic.show_points(ball.player_id)
			explode()
	effect.emit(ball, type)

func explode():
	has_exploded = true
	Animations.play("Explosion")
	for body in ExplosionLogic.get_overlapping_bodies():
		if !body.get_parent().has_method("explode"):
			body.toggle_visible(false)

func _on_brick_logic_toggled_visible(toggled_visible: bool):
	if toggled_visible:
		has_exploded = false
		Animations.play("RESET")

func chain_reaction():
	for body in ExplosionLogic.get_overlapping_bodies():
		if body.get_parent().has_method("explode") and !body.get_parent().has_exploded:
			body.get_parent().explode()
