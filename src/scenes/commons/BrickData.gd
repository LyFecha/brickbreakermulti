class_name BrickData

@export var type : E.BrickType
@export var variant : int
@export var position : Vector2
@export var rotation : int

func _init(type_: E.BrickType, variant_: int, position_: Vector2, rotation_: int):
	self.type = type_
	self.variant = variant_
	self.position = position_
	self.rotation = rotation_

func save() -> Dictionary:
	return {
		"type": type,
		"variant": variant,
		"position.x": position.x,
		"position.y": position.y,
		"rotation": rotation,
	}

static func load_(d: Dictionary) -> BrickData:
	return BrickData.new(d.type, d.variant, Vector2(d["position.x"], d["position.y"]), d.rotation)
