extends Node

const COLORS := [Color("0080ffff"), Color("ff0000ff")]

const LEVEL_NAMES := [
	"Bienvenue",
	"Amusez-vous bien",
	"Dur comme fer",
	"La fenêtre",
	"Ça colle !",
	"Les hachures",
	"Je vois triple !",
	"Trésors aux bords",
	"La guêpe territoriale",
	"C'est un mur porteur ça ?",
	"XXL à XXS",
	"Tir de précision",
	"Au temps pour moi",
	"Sécurités, mais qui marchent une fois sur deux",
	"Le coeur instable",
	"Les explosions, c'est bien",
	"Ce niveau est tellement bien qu'on le referait en boucle",
	"La montre",
	"Le mur du son",
	"Les chaînes",
	"Un trésor bien gardé",
	"La solution n'est pas la clé",
	"Bouche-trous",
	"Le puit",
	"Hypnotique",
	"Les gemmes aveuglantes",
	"Rétrécir pour se faufiler",
	"Déformation",
	"La lumière au bout du tunnel",
	"Récursif",
]

@export var BrickSounds := {
	E.BrickSound.CRUMBLE: [
		"res://src/assets/sfx/bricks/brick.ogg",
	],
	E.BrickSound.GOLD: [
		"res://src/assets/sfx/bricks/gold.ogg",
	],
	E.BrickSound.STICKY: [
		"res://src/assets/sfx/bricks/sticky.ogg",
	],
	E.BrickSound.METAL: [
		"res://src/assets/sfx/bricks/metal.ogg",
	],
	E.BrickSound.JEWEL: [
		"res://src/assets/sfx/bricks/jewel.ogg",
	],
	E.BrickSound.MUD: [
		"res://src/assets/sfx/bricks/mud.ogg",
	],
	E.BrickSound.GLASS: [
		"res://src/assets/sfx/bricks/glass.ogg",
	],
	E.BrickSound.LEAF: [
		"res://src/assets/sfx/bricks/leaf.ogg",
	],
	E.BrickSound.AIR: [
		"res://src/assets/sfx/bricks/air.ogg",
	],
	E.BrickSound.EXPLOSION: [
		"res://src/assets/sfx/bricks/explosion.ogg",
	],
	E.BrickSound.KEYCHAIN: [
		"res://src/assets/sfx/bricks/keychain.ogg",
	],
	E.BrickSound.UNLOCK: [
		"res://src/assets/sfx/bricks/unlock.ogg",
	],
	E.BrickSound.CEMENT: [
		"res://src/assets/sfx/bricks/cement1.ogg",
		"res://src/assets/sfx/bricks/cement2.ogg",
	],
	E.BrickSound.CURSE: [
		"res://src/assets/sfx/bricks/curse1.ogg",
		"res://src/assets/sfx/bricks/curse2.ogg",
		"res://src/assets/sfx/bricks/curse3.ogg",
	],
	E.BrickSound.LASER: [
		"res://src/assets/sfx/bricks/laser1.ogg",
		"res://src/assets/sfx/bricks/laser2.ogg",
	],
}

@export var VariantEnumByType := {
	E.BrickType.CLASSIC: E.ClassicVariant,
	E.BrickType.OWNER: E.OwnerVariant,
	E.BrickType.GROW: E.GrowVariant,
	E.BrickType.TIMING: E.TimingVariant,
	#E.BrickType.DIRECTIONAL:E.DirectionalVariant,
	E.BrickType.FAST: E.FastVariant,
	E.BrickType.SWELL: E.FastVariant,
}

@export var BrickByType := [
	"HPBrick",
	"GoldBrick",
	"StickyBrick",
	"SolidBrick",
	"MultiBallsBrick",
	"OwnerBrick",
	"GrowBrick",
	"TimingBrick",
	"ExplosiveBrick",
	"RedoBrick",
	"FastBrick",
	"KeyBrick",
	"DoorBrick",
	"InvincibilityBrick",
	"CurseBrick",
	"SwellBrick",
	"BeamBrick",
]
