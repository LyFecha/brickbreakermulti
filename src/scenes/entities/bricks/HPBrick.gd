extends Brick

var textures : Array[Texture2D] = [
	preload("res://src/assets/sprites/bricks/Brick1.png"),
	preload("res://src/assets/sprites/bricks/Brick2.png"),
	preload("res://src/assets/sprites/bricks/Brick3.png"),
	preload("res://src/assets/sprites/bricks/Brick4.png"),
	preload("res://src/assets/sprites/bricks/Brick5.png")
]

@export var brick_hp : int :
	get:
		if has_node("BrickLogic"):
			return $BrickLogic.brick_hp
		return 1
	set(new):
		if has_node("BrickLogic"):
			$BrickLogic.brick_hp = new
		brick_hp = new
		set_sprite(brick_hp)

func set_sprite(index: int):
	if index:
		if Sprite:
			Sprite.texture = textures[index - 1]
		else:
			set_sprite.call_deferred(index)

func set_variant(variant: int):
	brick_hp = variant + 1

func _on_brick_logic_new_hp(hp: int):
	set_sprite(hp)
