extends Node

func do_effect(type: E.BrickEffect, ball: Ball, level: Level):
	match type:
		E.BrickEffect.STICKY:
			ball.stick()
		E.BrickEffect.MULTI_BALLS:
			var new_ball_1 := ball.duplicate() as Ball
			var new_ball_2 := ball.duplicate() as Ball
			new_ball_1.linear_velocity = ball.linear_velocity.rotated(2 * PI / 3)
			new_ball_2.linear_velocity = ball.linear_velocity.rotated(-2 * PI / 3)
			new_ball_1.bar = ball.bar
			new_ball_2.bar = ball.bar
			new_ball_1.follow_beam = ball.follow_beam
			new_ball_2.follow_beam = ball.follow_beam
			level.add_child.call_deferred(new_ball_1)
			level.add_child.call_deferred(new_ball_2)
			level.balls[ball.player_id].push_back(new_ball_1)
			level.balls[ball.player_id].push_back(new_ball_2)
			if ball.sticky:
				new_ball_1.stick.call_deferred()
				new_ball_2.stick.call_deferred()
			if ball.current_scale_multiplier != 1:
				new_ball_1.current_scale_multiplier = 1
				new_ball_2.current_scale_multiplier = 1
				new_ball_1.change_scale.call_deferred(ball.current_scale_multiplier - 1)
				new_ball_2.change_scale.call_deferred(ball.current_scale_multiplier - 1)
		E.BrickEffect.FAST:
			ball.change_speed(1.35)
			level.bars[ball.player_id].change_speed(1.35)
		E.BrickEffect.SLOW:
			ball.change_speed(0.75)
			level.bars[ball.player_id].change_speed(0.75)
		E.BrickEffect.REDO:
			for child in level.get_children():
				if child is Brick:
					var brick := child as Brick
					if brick.Logic.brick_effect != E.BrickEffect.REDO and brick.Logic.points_type != E.PointType.GOLD:
						brick.Logic.toggle_visible(true)
		E.BrickEffect.GROW:
			level.bars[ball.player_id].change_scale(0.5)
		E.BrickEffect.SHRINK:
			level.bars[ball.player_id].change_scale(-0.25)
		E.BrickEffect.BEAM:
			level.bars[ball.player_id].set_beam(true)
			for player_ball: Ball in level.balls[ball.player_id]:
				player_ball.follow_beam = true
		E.BrickEffect.KEY:
			ball.add_key()
		E.BrickEffect.INVINCIBILITY:
			level.Walls.invincibility(ball.player_id)
		E.BrickEffect.CURSE:
			level.bars[ball.player_id].curse(true)
		E.BrickEffect.SWELL:
			ball.change_scale(0.75)
		E.BrickEffect.SLIM:
			ball.change_scale(-0.375)
