extends Node2D

@export var direction := Vector2.ZERO

func _physics_process(delta: float):
	for ball: Ball in ($ObstacleLogic as ObstacleLogic).get_overlapping_bodies():
		ball.apply_force(delta * 200000 * direction)
