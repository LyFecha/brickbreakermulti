extends Node2D

@onready var ball: Ball = $Ball

func _on_key_effect(ball: Ball, type: E.BrickEffect) -> void:
	ball.add_key()


func _on_door_effect(ball: Ball, type: E.BrickEffect) -> void:
	pass # Replace with function body.
