extends ConfirmationDialog

func _process(_delta: float):
	if visible && get_index() != get_parent().get_child_count() - 1:
		get_parent().move_child(self, get_parent().get_child_count() - 1)
		
func display_dialog(message: String) -> Button:
	popup_centered_clamped(Vector2(1000,200), 0.9)
	dialog_text = message
	get_ok_button().set_action_mode(BaseButton.ACTION_MODE_BUTTON_RELEASE)
	get_cancel_button().set_action_mode(BaseButton.ACTION_MODE_BUTTON_RELEASE)
	return get_ok_button()
