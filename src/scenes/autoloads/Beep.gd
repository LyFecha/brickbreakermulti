extends Node

@onready var Animations : AnimationPlayer = $AnimationPlayer


func play():
	Animations.play("Timing")
	
func stop():
	Animations.stop()
