extends Node2D
class_name Bar2

@export var player_id := 0 :
	set(id):
		set_color(C.COLORS[id])
		player_id = id

const VELOCITY_DECELERATION := 6
const VELOCITY_BOOST := 150
const BALL_TARGET_SPEED := 1000.0

@onready var Extension1 : AnimatableBody2D = $Extension1
@onready var Extension1Sprite : Sprite2D = $Extension1/Extension
@onready var Extension1Collision : CollisionPolygon2D = $Extension1/ExtensionCollision1
@onready var Extension2 : AnimatableBody2D = $Extension2
@onready var Extension2Sprite : Sprite2D = $Extension2/Extension
@onready var Extension2Collision : CollisionPolygon2D = $Extension2/ExtensionCollision2
@onready var Base : AnimatableBody2D = $Base
@onready var BaseSprite : Sprite2D = $Base/Base
@onready var BaseCollision : CollisionShape2D = $Base/BaseCollision
@onready var Path : Path2D = $Path
@onready var Follow : PathFollow2D = $Path/Follow
@onready var RayCast : RayCast2D = $Path/Follow/RayCast
@onready var BallPoint : Node2D = $Path/Follow/BallPoint
@onready var ScaleChangeTimer : Timer = $ScaleChangeTimer
@onready var Animations : AnimationPlayer = $Animations

@onready var base_scale : Vector2 = Base.scale
@onready var extension_scale : Vector2 = Extension1.scale
@onready var base_collision_scale : Vector2 = BaseCollision.scale
@onready var extension_collision_scale : Vector2 = Extension1Collision.scale
@onready var path_scale : Vector2 = Path.scale

var velocity := Vector2.ZERO
var extension_animation_speed := 0

func _ready():
	Animations.play("ExtensionChange", -1, 0)
	extension_animation_speed = 0
	Follow.progress_ratio = 0.35

func _physics_process(delta: float):
	if Input.is_action_pressed("down{0}".format([player_id + 1]), true):
		velocity += Vector2(0, VELOCITY_BOOST * delta)
	if Input.is_action_pressed("up{0}".format([player_id + 1]), true):
		velocity -= Vector2(0, VELOCITY_BOOST * delta)
	if Input.is_action_pressed("back{0}".format([player_id + 1]), true) and !has_attached_ball():
		if Animations.is_playing() and Animations.current_animation_position >= Animations.current_animation_length - delta:
			if extension_animation_speed != 0:
				extension_animation_speed = 0
				Animations.play("ExtensionChange", -1, extension_animation_speed)
		elif Animations.is_playing():
			if extension_animation_speed != 1:
				extension_animation_speed = 1
				Animations.play("ExtensionChange", -1, extension_animation_speed)
	else:
		if Animations.is_playing() and Animations.current_animation_position <= delta:
			if extension_animation_speed != 0:
				extension_animation_speed = 0
				Animations.play("ExtensionChange", -1, extension_animation_speed)
		elif Animations.is_playing() and extension_animation_speed != -1:
			if extension_animation_speed != -1:
				extension_animation_speed = -1
				Animations.play("ExtensionChange", -1, extension_animation_speed)
	if Input.is_action_just_pressed("accept{0}".format([player_id + 1]), true) and BallPoint.get_child_count() > 0:
		var ball : Ball = BallPoint.get_child(0)
		ball.linear_velocity += RayCast.get_collision_normal() * BALL_TARGET_SPEED
		var old_position := ball.global_position
		BallPoint.remove_child(ball)
		get_parent().add_child(ball)
		ball.global_position = old_position
		ball.unattached()
	var collision := Base.move_and_collide(velocity)
	if collision:
		velocity = collision.get_collider_velocity()
	velocity *= 1 - (delta * VELOCITY_DECELERATION)
	Follow.progress = clamp(Follow.progress - velocity.y, 0, 250.47)

func set_positon(new_pos: Vector2):
	Base.global_position = new_pos

func set_color(color: Color):
	$Base/BaseCollision.debug_color = Color(color, 0.3)
	
func attach_ball(ball: Ball):
	Animations.play("ExtensionChange", -1, 0)
	extension_animation_speed = 0
	BallPoint.scale = Vector2.ONE if !player_id else Vector2(-1, 1)
	if ball.get_parent():
		ball.get_parent().remove_child.call_deferred(ball)
	BallPoint.add_child.call_deferred(ball)
	ball.position = Vector2.ZERO
	ball.attached = true

func has_attached_ball():
	return BallPoint.get_child_count()
	
func get_ball_respawn_position():
	return Follow.global_position

func set_path_point_closest_to(ball: Ball):
	var curve := Path.curve
	var path_transform := Path.global_transform
	var closest_offset := curve.get_closest_offset(ball.position * path_transform)
	Follow.progress = closest_offset

func change_scale(new_scale: float):
	Extension1.apply_scale(Vector2(1, new_scale))
	Extension2.apply_scale(Vector2(1, new_scale))
	Base.apply_scale(Vector2(new_scale, new_scale))
	Extension1Collision.apply_scale(Vector2(new_scale, 1))
	Extension2Collision.apply_scale(Vector2(new_scale, 1))
	BaseCollision.apply_scale(Vector2(new_scale, 1))
	Path.apply_scale(Vector2(1, new_scale))
	ScaleChangeTimer.start()
	
func _reset_scale():
	Extension1.scale = extension_scale
	Extension2.scale = extension_scale
	Base.scale = base_scale
	Extension1Collision.scale = extension_collision_scale
	Extension2Collision.scale = extension_collision_scale
	BaseCollision.scale = base_collision_scale
	Path.scale = path_scale
