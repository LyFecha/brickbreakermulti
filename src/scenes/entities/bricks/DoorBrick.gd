extends Brick

func _on_brick_logic_effect(ball: Ball, type: E.BrickEffect):
	if type == E.BrickEffect.DOOR:
		var had_key := ball.remove_key_or_nothing()
		if had_key:
			points.emit(ball.player_id, E.PointType.BRICK, 20)
			Logic.toggle_visible(false)
