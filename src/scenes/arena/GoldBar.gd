extends Control

@export var id := 1

@onready var Gauge : TextureRect = $Gauge
@onready var Animations : AnimationPlayer = $Animations

func _ready():
	var library := Animations.get_animation_library("")
	var gauge_animation := Animations.get_animation("Gauge")
	library.add_animation("Gauge"+str(id), gauge_animation.duplicate(true))
	(Gauge.material as ShaderMaterial).set_shader_parameter("offset", 0.9999)

func change_gauge(current: int, total: int):
	var gauge_animation := Animations.get_animation("Gauge"+str(id))
	gauge_animation.track_set_key_value(0, 0, (Gauge.material as ShaderMaterial).get_shader_parameter("offset") )
	gauge_animation.track_set_key_value(0, 1, 1 - (current / float(total)) )
	gauge_animation.track_set_key_value(1, 0, Gauge.size)
	gauge_animation.track_set_key_value(1, 1, Vector2(size.x * (current / float(total)) / Gauge.scale.x, size.y / Gauge.scale.y))
	Animations.stop(true)
	Animations.play("Gauge"+str(id))
