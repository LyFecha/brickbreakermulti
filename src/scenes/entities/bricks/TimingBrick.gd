extends Brick

@onready var Animations: AnimationPlayer = $AnimationPlayer

@export var is_green : bool = false :
	set(it_is):
		make_green(it_is)
		is_green = it_is

func _ready():
	make_green(is_green)
	Beep.play()

func make_green(it_is: bool):
	$AnimationPlayer.play("Timing")
	var half_animation = $AnimationPlayer.current_animation_length / 2
	$AnimationPlayer.advance((half_animation if it_is else 0) - $AnimationPlayer.current_animation_position)
	$Sprite.texture = load("res://src/assets/sprites/bricks/TimingBrick{0}.png".format([1 if it_is else 0]))

func set_variant(variant: int):
	is_green = variant


func _on_brick_logic_toggled_visible(is_visible_: bool) -> void:
	if !is_visible_:
		Animations.stop()
		Logic.set_collision_layer_value(3, false)
	else:
		Animations.play("Timing")
