extends Resource
class_name StateType

const SAVE_VERSION := "1.4.0"

const SAVE_PATH := "user://custom_levels.save"
const LEVEL_PREVIEW_PATH := "user://level{0}.png"
const BASE_LEVEL_MAX := 30

# Flag
@export var have_seen_controls := false
@export var have_seen_editor_controls := false
@export var have_loaded_data := false
@export var is_fullscreen := false

# In game
@export var current_level : int = 1
@export var mode : E.Mode = E.Mode.DUEL
@export var total_score : Array[int] = [0, 0]
@export var ai_level : Array[int] = [0, 0]

# Dual Mode
@export var level_order : Array[int] = [1]
@export var level_index : int = 0

# Editor & Custom Levels
var level : LevelData = LevelData.new()
var custom_level_index : int = 0
var is_custom_loaded := false

# Non-persistent options
var handicap : Array[Dictionary] = [
	{"speed": 0, "size": 0, "gold": 0},
	{"speed": 0, "size": 0, "gold": 0},
]

# Persistent
@export var have_seen_how_to_play := false
var custom_levels : Array[LevelData] = []
var key_bindings : Dictionary
var is_arrow_visible : bool = true
var show_points : bool = false

func save_data():
	var file := FileAccess.open(SAVE_PATH, FileAccess.WRITE)
	file.store_line(SAVE_VERSION)
	file.store_line(str(show_points))
	file.store_line(str(is_arrow_visible))
	file.store_line(JSON.stringify(key_bindings))
	file.store_line(str(have_seen_how_to_play))
	for i in range(len(custom_levels)):
		var cl := custom_levels[i]
		var save := cl.save()
		file.store_line(save)
		
func load_data():
	if FileAccess.file_exists(SAVE_PATH) and !have_loaded_data:
		var file := FileAccess.open(SAVE_PATH, FileAccess.READ)
		var version := file.get_line()
		print("Version de sauvegarde : ", version)
		if version == "1.0.0": return load_data_1_0_0(file)
		if version == "1.1.0": return load_data_1_1_0(file)
		if version == "1.2.0": return load_data_1_2_0(file)
		if version == "1.3.0": return load_data_1_3_0(file)
		if version == "1.4.0": return load_data_1_4_0(file)
		print("Erreur: version de sauvegarde incompatible")
		
func load_data_1_4_0(file: FileAccess):
	show_points = file.get_line() == "true"
	load_data_1_3_0(file)
		
func load_data_1_3_0(file: FileAccess):
	load_data_1_2_0(file, true)
		
func load_data_1_2_0(file: FileAccess, post_1_3_0: bool = false):
	is_arrow_visible = file.get_line() == "true"
	load_data_1_1_0(file, post_1_3_0)

func load_data_1_1_0(file: FileAccess, post_1_3_0: bool = false):
	key_bindings = JSON.parse_string(file.get_line())
	if key_bindings:
		for key in key_bindings:
			var values := (key_bindings[key] as String).split(";")
			var event := InputEventKey.new()
			if len(values) == 3:
				event.key_label = int(values[0]) as Key
				event.keycode = int(values[1]) as Key
				event.physical_keycode = int(values[2]) as Key
			InputMap.action_erase_events(key)
			InputMap.action_add_event(key, event)
	load_data_1_0_0(file, post_1_3_0)

func load_data_1_0_0(file: FileAccess, post_1_3_0: bool = false):
	have_seen_how_to_play = file.get_line() == "true"
	custom_levels = []
	while file.get_position() < file.get_length():
		var line := file.get_line()
		print(line)
		custom_levels.append(LevelData.load_(line, post_1_3_0))
	have_loaded_data = true
	
func calculate_symmetry_variant(type: E.BrickType, variant: int, symmetry: E.SymmetryOption) -> int:
	match [type, variant]:
		[E.BrickType.OWNER, var v] when v > E.OwnerVariant.NONE and (symmetry == E.SymmetryOption.H or symmetry == E.SymmetryOption.CENTRAL):
			return E.OwnerVariant.BLUE if v == E.OwnerVariant.RED else E.OwnerVariant.RED
		#[E.BrickType.DIRECTIONAL, _] when symmetry == E.SymmetryOption.H and rotation != 90 \
										#or symmetry == E.SymmetryOption.HV and rotation != 0 \
										#or E.SymmetryOption.CENTRAL:
			#return E.DirectionalVariant.RIGHT_WALL if variant else E.DirectionalVariant.LEFT_WALL
	return variant
