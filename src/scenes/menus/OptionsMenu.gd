extends Control

const BOOL_LABEL : Array[String] = ["Non", "Oui"]
const SPEED_LABEL : Array[String] = ["Très lent", "Lent", "Normal", "Rapide", "Très rapide"]
const SIZE_LABEL : Array[String] = ["Très petite", "Petite", "Normal", "Grande", "Très grande"]
const GOLD_LABEL : Array[String] = ["0", "1", "2", "3", "4", "5", "6", "7"]

var current_button : Button

@export var State : StateType
@export var change_scene_on_quit := true

@onready var buttons : Array[Button] = [$Controles/Player1/Top, $Controles/Player1/Bottom, $Controles/Player1/Switch, $Controles/Player1/Launch, $Controles/Player1/Pause, $Controles/Player2/Top, $Controles/Player2/Bottom, $Controles/Player2/Switch, $Controles/Player2/Launch, $Controles/Player2/Pause]
@onready var actions : Array[String] = ["up1", "down1", "back1", "accept1", "pause1", "up2", "down2", "back2", "accept2", "pause2"]
@onready var action_to_button : Dictionary = {
	"up1": $Controles/Player1/Top,
	"down1": $Controles/Player1/Bottom,
	"back1": $Controles/Player1/Switch,
	"accept1": $Controles/Player1/Launch,
	"pause1": $Controles/Player1/Pause,
	"up2": $Controles/Player2/Top,
	"down2": $Controles/Player2/Bottom,
	"back2": $Controles/Player2/Switch,
	"accept2": $Controles/Player2/Launch,
	"pause2": $Controles/Player2/Pause,
}

@onready var Press : Panel = $Controles/Press
@onready var Controles : Button = $TabBar/Controles
@onready var InGame : Button = $TabBar/InGame
@onready var Tabs : Array[Panel] = [$Controles, $InGame]

@onready var Arrow : Button = $InGame/Arrow
@onready var Points: Button = $InGame/Points


func _ready() -> void:
	Controles.button_pressed = true
	show_tab(0)
	
	for button in buttons:
		button.pressed.connect(_on_button_pressed.bind(button))
	_update_labels()
	Press.visible = false
	
	Arrow.text = BOOL_LABEL[int(State.is_arrow_visible)]
	Points.text = BOOL_LABEL[int(State.show_points)]
	
	update_handicaps()

func _on_button_pressed(button: Button) -> void:
	current_button = button
	Press.visible = true

func _on_back_pressed():
	current_button = null
	Press.visible = false
	
func _input(event: InputEvent) -> void:
	if !current_button:
		return
		
	if event is InputEventKey:
		var all_ies : Dictionary = {}
		for ia in InputMap.get_actions():
			for iae in InputMap.action_get_events(ia):
				all_ies[iae.as_text()] = ia
		
		if all_ies.keys().has(event.as_text()):
			InputMap.action_erase_events(all_ies[event.as_text()])
		
		var action_ind := buttons.find(current_button)
		var action := actions[action_ind]
		InputMap.action_erase_events(action)
		InputMap.action_add_event(action, event)
		
		current_button = null
		Press.visible = false
		
		_update_labels()
		get_viewport().set_input_as_handled()
		
func _update_labels() -> void:
	for action_str in action_to_button:
		var button : Button = action_to_button[action_str]
		var events := InputMap.action_get_events(action_str)
		button.get_child(0).text = events[0].as_text() if !events.is_empty() else ""

func _on_menu_back_pressed():
	save()
	if change_scene_on_quit:
		get_tree().change_scene_to_file("res://src/scenes/menus/MainMenu.tscn")
		return
	visible = false

func save():
	if !State.key_bindings:
		State.key_bindings = {
			"up1": "",
			"down1": "",
			"back1": "",
			"accept1": "",
			"pause1": "",
			"up2": "",
			"down2": "",
			"back2": "",
			"accept2": "",
			"pause2": "",
		}
	for action in State.key_bindings:
		var events := InputMap.action_get_events(action)
		if len(events):
			var event := events[0] as InputEventKey
			State.key_bindings[action] = str(event.key_label) + ";" + str(event.keycode) + ";" + str(event.physical_keycode)
	State.save_data()

func _on_reset_pressed():
	var ok_button := Confirm.display_dialog("Etes-vous sûr de remettres les contrôles par défaut ?")
	ok_button.pressed.connect(_reset)
	
func _reset():
	InputMap.load_from_project_settings()
	_update_labels()

func show_tab(tab_index_to_show: int):
	for tab_index in range(len(Tabs)):
		Tabs[tab_index].visible = tab_index == tab_index_to_show

func _on_arrow_pressed():
	State.is_arrow_visible = !State.is_arrow_visible
	Arrow.text = BOOL_LABEL[int(State.is_arrow_visible)]

func _on_points_pressed() -> void:
	State.show_points = !State.show_points
	Points.text = BOOL_LABEL[int(State.show_points)]
	
func _on_more_or_less_pressed(is_more: bool, key: String, player_id: int) -> void:
	var length : int
	var offset : int
	match key:
		"speed", "size":
			length = 5
			offset = 2
		"gold":
			length = 8
			offset = 0

	State.handicap[player_id][key] = (State.handicap[player_id][key] + (1 if is_more else -1) + length + offset) % length - offset
	update_handicaps()
	
func update_handicaps():
	for player_id in range(len(State.handicap)):
		for key in State.handicap[player_id].keys():
			var label : String
			var label_array : Array[String]
			var offset : int
			match key:
				"speed":
					label = "Speed"
					label_array = SPEED_LABEL
					offset = 2
				"size":
					label = "Size"
					label_array = SIZE_LABEL
					offset = 2
				"gold":
					label = "Gold"
					label_array = GOLD_LABEL
					offset = 0
			if label:
				$InGame\
					.get_node("Player{0}".format([player_id+1]))\
					.get_node(label)\
					.text = label_array[State.handicap[player_id][key] + offset]
