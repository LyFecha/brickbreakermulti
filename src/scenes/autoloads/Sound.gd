extends Node
class_name SoundClass

const BASE_VOLUME := 8.0

@onready var Sound : AudioStreamPlayer = $ButtonSound

@onready var attenuation_timers: Array[Timer] = [$AttenuationTimer0, $AttenuationTimer1]

func _ready():
	# when _ready is called, there might already be nodes in the tree, so connect all existing buttons
	connect_buttons(get_tree().root)
	get_tree().node_added.connect(_on_SceneTree_node_added)

# SOUND FOR BUTTONS #

func _on_SceneTree_node_added(node: Node):
	if node is BaseButton and !node.get_meta("silent", false):
		connect_to_button(node)

func _on_Button_pressed():
	Sound.pitch_scale = 0.9 + randf() * 0.2
	Sound.play()

func connect_buttons(root: Node):
	for child: Node in root.get_children():
		if child is BaseButton and !child.get_meta("silent", false):
			connect_to_button(child)
		connect_buttons(child)

func connect_to_button(button: BaseButton):
	button.pressed.connect(self._on_Button_pressed)

# BRICK SOUND MANAGER #

func play_sound(key: E.BrickSound, sound: AudioStreamPlayer2D, attenuated_sound: AudioStreamPlayer2D, attenuation: bool = false, player_id: int = 0):
	if key and C.BrickSounds.has(key):
		var sounds = C.BrickSounds[key]
		if len(sounds):
			var volume := 0
			if attenuation:
				volume = -24.0 * 4 * max(attenuation_timers[player_id].time_left, 0) ** 2
				attenuation_timers[player_id].start(0.5)
			var played_sound := sound if volume >= -8 else attenuated_sound
			played_sound.stream = load(sounds[randi_range(0, len(sounds) - 1)]) as AudioStreamOggVorbis
			played_sound.pitch_scale = 0.7 + randf() * 0.6
			played_sound.volume_db = volume + BASE_VOLUME
			played_sound.play()

func _on_attenuation_timer_0_timeout() -> void:
	attenuation_timers[0].stop()

func _on_attenuation_timer_1_timeout() -> void:
	attenuation_timers[0].stop()
