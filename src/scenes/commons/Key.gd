@tool
extends Control

@onready var Text : Label = $Text
@onready var Arrow : Line2D = $Arrow

@export var text := "A" :
	set(new):
		if Text:
			Text.text = new
		text = new
@export_range(0, 1.25) var text_size : float = 1 :
	set(new):
		if Text:
			Text.add_theme_font_size_override("font_size", int(new * 64.0))
		text_size = new
@export_range(-360, 360) var arrow_rotation : float = 0.0 :
	set(new):
		if Arrow:
			Arrow.rotation_degrees = new
			if new:
				Arrow.visible = true
			else:
				Arrow.visible = false
		arrow_rotation = new

func _ready():
	set_process_input(not Engine.is_editor_hint())
	Text.text = text
	Text.add_theme_font_size_override("font_size", int(text_size * 64.0))
	Arrow.rotation_degrees = arrow_rotation
	if arrow_rotation:
		Arrow.visible = true
	else:
		Arrow.visible = false
