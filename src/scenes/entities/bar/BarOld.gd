extends AnimatableBody2D
class_name BarOld

@export var player_id = 0
@export var pole : RigidBody2D

const VELOCITY_DECELERATION = 6
const VELOCITY_BOOST = 150

var velocity = Vector2.ZERO

func _ready():
	pole = $BallPole

func _physics_process(delta: float):
	if Input.is_action_pressed("down{0}".format([player_id + 1]), true):
		velocity += Vector2(0, VELOCITY_BOOST * delta)
	if Input.is_action_pressed("up{0}".format([player_id + 1]), true):
		velocity -= Vector2(0, VELOCITY_BOOST * delta)
	if Input.is_action_just_pressed("accept{0}".format([player_id + 1]), true) and $BallJoint.node_b:
		var ball : Ball = get_node($BallJoint.node_b)
		ball.linear_velocity = ($BallPole.global_position - ball.global_position).normalized()
		$BallJoint.node_b = NodePath("")
	var collision := move_and_collide(velocity)
	if collision:
		velocity = collision.get_collider_velocity()
	velocity *= 1 - (delta * VELOCITY_DECELERATION)
	$BallPole.position = ($BallPole.position - velocity).clamp(Vector2(0, -100), Vector2(0, 100))

func set_color(color: Color):
	$Collision.debug_color = color
	
func attach_ball(ball: Ball):
	$BallJoint.node_b = ball.get_path()
	
func get_ball_respawn_position():
	return $BallPole/BallRespawn.global_position
