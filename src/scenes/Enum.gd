extends Node
class_name E

enum PointType {
	BRICK,
	GOLD,
	GOAL,
	END_BONUS,
}

enum BrickEffect {
	NONE,
	STICKY,
	MULTI_BALLS,
	OWNER,
	FAST,
	SLOW,
	REDO,
	EXPLOSIVE,
	GROW,
	SHRINK,
	BEAM,
	KEY,
	DOOR,
	INVINCIBILITY,
	CURSE,
	SWELL,
	SLIM,
}

enum BrickSound {
	NONE,
	CRUMBLE,
	GOLD,
	STICKY,
	METAL,
	JEWEL,
	MUD,
	GLASS,
	LEAF,
	AIR,
	EXPLOSION,
	KEYCHAIN,
	UNLOCK,
	CEMENT,
	CURSE,
	LASER,
}

enum Mode {
	DUEL,
	MATCH,
	TESTING,
}

enum SymmetryOption {
	H,
	HV,
	CENTRAL,
	LENGTH,
}

enum BrickType {
	CLASSIC,
	GOLD,
	STICKY,
	SOLID,
	MULTIBALLS,
	OWNER,
	GROW,
	TIMING,
	EXPLOSIVE,
	REDO,
	FAST,
	KEY,
	DOOR,
	INVINCIBILITY,
	CURSE,
	SWELL,
	BEAM,
	#DIRECTIONAL,
}

# Variants #

enum ClassicVariant {
	HP1,
	HP2,
	HP3,
	HP4,
	HP5,
	LENGTH,
}

enum OwnerVariant {
	NONE,
	BLUE,
	RED,
	LENGTH,
}

enum GrowVariant {
	GROW,
	SHRINK,
	LENGTH,
}

enum TimingVariant {
	PURPLE,
	GREEN,
	LENGTH,
}

enum FastVariant {
	FAST,
	SLOW,
	LENGTH,
}

#enum DirectionalVariant {
	#RIGHT_WALL,
	#LEFT_WALL,
	#LENGTH,
#}
