extends Control

@export var State: StateType

const GARAGE := Vector2(-48, -80)
const OFFSET := Vector2(0, 80)
const MIN_POSITION := Vector2(0, 144)
const MAX_POSITION := Vector2(1920, 1016)
const CENTER := (MAX_POSITION + MIN_POSITION) / 2

const BRICKS : Array[String] = [
	"Brick1",
	"GoldBrick",
	"StickyBrick",
	"SolidBrick",
	"MultiBallsBrick",
	"OwnerBrick",
	"GrowBrick",
	"TimingBrick0",
	"ExplosiveBrick",
	"RedoBrick",
	"FastBrick",
	"KeyBrick",
	"DoorBrick",
	"InvincibilityBrick",
	"CurseBrick",
	"SwellBrick",
	"BeamBrick",
]
const BRICKS_TOOLTIP : Array[String] = [
	"Brique :\nUne simple brique.\nEtats : change le nombre de coup requis pour le détruire",
	"Brique d'or :\nNécessaire pour terminer la partie",
	"Brique collante :\nColle le palet à la barre",
	"Brique solide :\nIndestructible au minage",
	"Brique multiple :\nDuplique le palet en 3 exemplaires",
	"Brique propriétaire :\nLaisse passer le palet qui l'a touché\nEtats : Change le propriétaire de la brique",
	"Brique grand/petit :\nAgrandit/rétrécit la barre\nEtats : Change l'effet",
	"Brique rythmique :\nIndestructible au minage, mais le palet peut y passer au travers tous les 5 secondes\nEtats : Change la couleur et inverse les moments de passage au travers",
	"Brique explosive :\nDétruit les briques direct autour (diagonale incluse)",
	"Brique à refaire :\nFait réapparaître les briques (sauf or et à refaire)",
	"Brique rapide/lent :\nAccélère/ralentit le palet\nEtats : Change l'effet",
	"Brique clé :\nPermet d'ouvrir une porte",
	"Brique porte :\nIndestructible au minage sauf avec une clé",
	"Brique d'invincibilité :\nProtège des buts contre son camps",
	"Brique maudite :\nInverse les contrôles de la barre",
	"Brique gros/mince :\nAgrandit/rétrécit le palet\nEtats : Change l'effet",
	"Brique laser :\nForce la trajectoire du palet",
]
const CLASSIC_BRICK_TEXTURES := [preload("res://src/assets/sprites/bricks/Brick1.png"), preload("res://src/assets/sprites/bricks/Brick2.png"), preload("res://src/assets/sprites/bricks/Brick3.png"), preload("res://src/assets/sprites/bricks/Brick4.png"), preload("res://src/assets/sprites/bricks/Brick5.png")]
const OWNER_BRICK_OVERLAY := preload("res://src/assets/sprites/bricks/OwnerBrickOverlay.tres")
const GROW_BRICK_TEXTURES := [preload("res://src/assets/sprites/bricks/GrowBrick.png"), preload("res://src/assets/sprites/bricks/ShrinkBrick.png")]
const TIMING_BRICK_TEXTURES := [preload("res://src/assets/sprites/bricks/TimingBrick0.png"), preload("res://src/assets/sprites/bricks/TimingBrick1.png")]
const FAST_BRICK_TEXTURES := [preload("res://src/assets/sprites/bricks/FastBrick.png"), preload("res://src/assets/sprites/bricks/SlowBrick.png")]
const SWELL_BRICK_TEXTURES := [preload("res://src/assets/sprites/bricks/SwellBrick.png"), preload("res://src/assets/sprites/bricks/SlimBrick.png")]
const BrickArea := preload("res://src/scenes/editor/BrickArea.tscn")
const BrickChoice := preload("res://src/scenes/editor/BrickChoice.tscn")
const BrickChoiceGroup := preload("res://src/scenes/commons/ButtonGroup.tres")
var bricks_texture : Array[Texture2D] = []

@onready var Player : AnimationPlayer = $Player
@onready var Bricks : Node2D = $Level/Level/Bricks
@onready var Grid : GridClass = $Grid
@onready var Name : LineEdit = $Options/Name
@onready var Symmetry : Button = $Options/Symmetry
@onready var GoldToWin : SpinBox = $Options/GoldToWin
@onready var GridSnapX : SpinBox = $Options/GridSnapX
@onready var GridSnapY : SpinBox = $Options/GridSnapY
@onready var GridOffsetX : SpinBox = $Options/GridOffsetX
@onready var GridOffsetY : SpinBox = $Options/GridOffsetY
@onready var Help: Button = $Options/Help
@onready var MoreOptions : Button = $Toolbar/MoreOptions
@onready var BricksBox : HBoxContainer = $Toolbar/Bricks/Box
@onready var MouseBricks : Node2D = $MouseBricks
@onready var MouseBrick0 : Area2D = $MouseBricks/MouseBrick0
@onready var MouseBrick1 : Area2D = $MouseBricks/MouseBrick1
@onready var MouseBrick2 : Area2D = $MouseBricks/MouseBrick2
@onready var MouseBrick3 : Area2D = $MouseBricks/MouseBrick3
@onready var Error : Panel = $Error
@onready var ErrorLabel : Label = $Error/Label
@onready var ErrorTimer : Timer = $Error/Timer
@onready var EditValidated : Panel = $EditValidated
@onready var Export : Panel = $Export
@onready var Save : TextEdit = $Export/Save
@onready var ExportError : Label = $Export/Error
@onready var Import : Panel = $Import
@onready var PasteArea : TextEdit = $Import/PasteArea
@onready var ImportError : Label = $Import/Error
@onready var Exit : Panel = $Exit
@onready var Controls : Panel = $Controls
@onready var ControlsPage1 : Control = $Controls/Page1
@onready var ControlsPage2 : Control = $Controls/Page2

@onready var level := State.level

var controls_page := 0

var in_level := false
var current_brick_type : E.BrickType = E.BrickType.CLASSIC
var current_brick_variant : int = 0
var brick_rotation : int = 0

func _ready():
	for i in range(len(BRICKS)):
		var brick_name := BRICKS[i]
		var texture_resource := load("res://src/assets/sprites/bricks/{0}.png".format([brick_name])) as CompressedTexture2D
		bricks_texture.push_back(texture_resource)
		var brick_choice := BrickChoice.instantiate() as Control
		var choice_button := brick_choice.get_child(0) as TextureButton
		var image_pressed := texture_resource.get_image()
		image_pressed.adjust_bcs(0.5, 1, 1)
		var texture_pressed := ImageTexture.create_from_image(image_pressed)
		choice_button.texture_normal = bricks_texture[i]
		choice_button.texture_pressed = texture_pressed

		if i == 0:
			choice_button.button_pressed = true
		choice_button.button_group = BrickChoiceGroup
		choice_button.pressed.connect(_brick_chosen.bind(i))
		choice_button.tooltip_text = BRICKS_TOOLTIP[i]
		BricksBox.add_child(brick_choice)
	build_level()
	Controls.visible = !State.have_seen_editor_controls
	ControlsPage1.visible = true
	ControlsPage2.visible = false
	_draw_grid()
	

func build_level():
	change_symmetry()
	Name.text = level.name
	GoldToWin.value = level.gold_to_win
	GridSnapX.value = level.snap.x
	GridSnapY.value = level.snap.y
	GridOffsetX.value = level.offset.x
	GridOffsetY.value = level.offset.y
	for b: BrickData in level.brick_data:
		b.position = b.position + OFFSET
		var x_prime = 2 * CENTER.x - b.position.x
		var y_prime = 2 * CENTER.y - b.position.y
		var bricks := [b] as Array[BrickData]
		if (level.symmetry == E.SymmetryOption.H or level.symmetry == E.SymmetryOption.HV) and x_prime != b.position.x:
			bricks.append(BrickData.new(b.type, State.calculate_symmetry_variant(b.type, b.variant, E.SymmetryOption.H), Vector2(x_prime, b.position.y), b.rotation))
			
		if (level.symmetry == E.SymmetryOption.HV) and y_prime != b.position.y:
			bricks.append(BrickData.new(b.type, State.calculate_symmetry_variant(b.type, b.variant, E.SymmetryOption.HV), Vector2(b.position.x, y_prime), b.rotation))
				
		if (level.symmetry == E.SymmetryOption.CENTRAL and (x_prime != b.position.x or y_prime != b.position.y)) or (level.symmetry == E.SymmetryOption.HV and x_prime != b.position.x and y_prime != b.position.y):
			bricks.append(BrickData.new(b.type, State.calculate_symmetry_variant(b.type, b.variant, E.SymmetryOption.CENTRAL), Vector2(x_prime, y_prime), b.rotation))
		
		for brick in bricks:
			var b_area := BrickArea.instantiate() as Area2D
			b_area.set_meta("variant", brick.variant)
			setup_brick_area(b_area, brick.type)
			Bricks.add_child(b_area)
			b_area.global_position = brick.position
			b_area.rotation_degrees = brick.rotation
		b.position = b.position - OFFSET

func _on_level_mouse_entered():
	in_level = true
	
func _on_level_mouse_exited():
	in_level = false

func _physics_process(_delta: float):
	var mouse_position := get_local_mouse_position().clamp(MIN_POSITION, MAX_POSITION)
	var fsnap := 8 * level.snap
	var foffset := 8 * level.offset
	var rounded_position := ((mouse_position - foffset - CENTER) / fsnap).round() * fsnap + foffset + CENTER
	var x_prime = 2 * CENTER.x - rounded_position.x
	var y_prime = 2 * CENTER.y - rounded_position.y
	MouseBrick0.position = GARAGE if !MouseBrick0.get_meta("active") else rounded_position
	MouseBrick1.position = GARAGE if !MouseBrick1.get_meta("active") else Vector2(x_prime, rounded_position.y)
	MouseBrick2.position = GARAGE if !MouseBrick2.get_meta("active") else Vector2(rounded_position.x, y_prime)
	MouseBrick3.position = GARAGE if !MouseBrick3.get_meta("active") else Vector2(x_prime, y_prime)
	var overlapping_areas := MouseBricks.get_children()\
		.map(func(mb): return [] if !mb.get_meta("active") else mb.get_overlapping_areas())\
		.reduce(func(acc, oas): acc.append_array(oas); return acc, [] as Array[Area2D]) as Array[Area2D]
	var overlapping_bodies := MouseBricks.get_children()\
		.map(func(mb): return [] if !mb.get_meta("active") else mb.get_overlapping_bodies())\
		.reduce(func(acc, obs): acc.append_array(obs); return acc, [] as Array[Node2D]) as Array[Node2D]
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) and !len(overlapping_areas) and !len(overlapping_bodies) and in_level:
		if level.validated:
			EditValidated.visible = true
			return
		var err := append_with_symmetry()
		match err:
			SYMMETRY_ERROR.NOT_SYMMETRICAL_BRICK_ON_Y_AXIS:
				Error.visible = true
				ErrorTimer.start()
				ErrorLabel.text = "Impossible de placer sur l'axe de symétrie.\nLa brique n'est pas symétrique horizontalement"
				return
			SYMMETRY_ERROR.NOT_SYMMETRICAL_BRICK_ON_X_AXIS:
				Error.visible = true
				ErrorTimer.start()
				ErrorLabel.text = "Impossible de placer sur l'axe de symétrie.\nLa brique n'est pas symétrique verticalement"
				return
			SYMMETRY_ERROR.NOT_SYMMETRICAL_BRICK_ON_EITHER_AXIS:
				Error.visible = true
				ErrorTimer.start()
				ErrorLabel.text = "Impossible de placer sur l'axe de symétrie.\nLa brique n'est pas symétrique horizontalement et/ou verticalement"
				return
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_RIGHT):
		if level.validated:
			EditValidated.visible = true
			return
		for area in overlapping_areas:
			level.brick_data = level.brick_data.filter(func(bd: BrickData): return bd.position != area.global_position - OFFSET)
			area.queue_free()
		
enum SYMMETRY_ERROR {
	OK,
	NOT_SYMMETRICAL_BRICK_ON_Y_AXIS,
	NOT_SYMMETRICAL_BRICK_ON_X_AXIS,
	NOT_SYMMETRICAL_BRICK_ON_EITHER_AXIS,
}
func append_with_symmetry() -> SYMMETRY_ERROR:
	var new_bricks : Array[BrickData] = [BrickData.new(current_brick_type, MouseBrick0.get_meta("variant"), MouseBrick0.position - OFFSET, brick_rotation)]
	
	if level.symmetry == E.SymmetryOption.H or level.symmetry == E.SymmetryOption.HV:
		if MouseBrick0.position.x == MouseBrick1.position.x and brick_rotation != 90 and MouseBrick0.get_meta("variant") != MouseBrick1.get_meta("variant"):
			return SYMMETRY_ERROR.NOT_SYMMETRICAL_BRICK_ON_Y_AXIS
		new_bricks.append(BrickData.new(current_brick_type, MouseBrick1.get_meta("variant"), MouseBrick1.position - OFFSET, brick_rotation))
		
		if level.symmetry == E.SymmetryOption.HV:
			if MouseBrick0.position.y == MouseBrick2.position.y and brick_rotation != 0 and MouseBrick0.get_meta("variant") != MouseBrick2.get_meta("variant"):
				return SYMMETRY_ERROR.NOT_SYMMETRICAL_BRICK_ON_Y_AXIS
			new_bricks.append(BrickData.new(current_brick_type, MouseBrick2.get_meta("variant"), MouseBrick2.position - OFFSET, brick_rotation))
			
	if level.symmetry == E.SymmetryOption.CENTRAL or level.symmetry == E.SymmetryOption.HV:
		if MouseBrick0.position.x == MouseBrick3.position.x and MouseBrick0.position.y == MouseBrick3.position.y and brick_rotation != 90 and MouseBrick0.get_meta("variant") != MouseBrick3.get_meta("variant"):
			return SYMMETRY_ERROR.NOT_SYMMETRICAL_BRICK_ON_EITHER_AXIS
		new_bricks.append(BrickData.new(current_brick_type, MouseBrick3.get_meta("variant"), MouseBrick3.position - OFFSET, brick_rotation))
		
	spawn_bricks()
	level.brick_data.append(
		new_bricks.reduce(
			func(acc: BrickData, b: BrickData):
				if acc == null:
					return b
				if b.position < acc.position:
					return b
				return acc
				\
		)
	)
	return SYMMETRY_ERROR.OK

func spawn_bricks():
	var position_placed := []
	for MB in MouseBricks.get_children():
		if MB.get_meta("active") and !MB.global_position in position_placed:
			var brick := MB.duplicate()
			brick.name = "B{0}V{1}X{2}Y{3}".format([current_brick_type, MB.get_meta("variant"), MB.position.x, MB.position.y])
			Bricks.add_child(brick)
			brick.global_position = MB.global_position
			brick.modulate = Color(1,1,1,1)
			brick.set_collision_layer_value(1, true)
			position_placed.append(MB.global_position)
	
func add_variant(type: E.BrickType, variant: int):
	current_brick_variant = variant
	MouseBrick0.set_meta("variant", variant)
	MouseBrick1.set_meta("variant", State.calculate_symmetry_variant(type, variant, E.SymmetryOption.H))
	MouseBrick2.set_meta("variant", State.calculate_symmetry_variant(type, variant, E.SymmetryOption.HV))
	MouseBrick3.set_meta("variant", State.calculate_symmetry_variant(type, variant, E.SymmetryOption.CENTRAL))
	for MB in MouseBricks.get_children():
		setup_brick_area(MB, type)
		
func setup_brick_area(b: Area2D, type: E.BrickType):
	b.set_meta("type", type)
	var Overlay := b.get_node("Overlay") as Polygon2D
	var Texture_ := b.get_node("Texture") as Polygon2D
	var v := b.get_meta("variant") as int
	match [type, v]:
		[E.BrickType.OWNER, var new_v] when new_v > E.OwnerVariant.NONE:
			Overlay.color = Color("ff0000ff") if v == E.OwnerVariant.RED else Color("0080ffff")
			Overlay.texture = OWNER_BRICK_OVERLAY
			Overlay.texture_offset = Vector2(18, 0)
			Overlay.scale.x = (-1 if v == E.OwnerVariant.RED else 1) * abs(Overlay.scale.x)
		[_, _]:
			Overlay.color = Color("00000000")
			Overlay.texture = null
			Overlay.texture_offset = Vector2.ZERO
			Overlay.scale.x = 1
	match type:
		E.BrickType.CLASSIC:
			Texture_.texture = CLASSIC_BRICK_TEXTURES[v]
		E.BrickType.GROW:
			Texture_.texture = GROW_BRICK_TEXTURES[v]
		E.BrickType.TIMING:
			Texture_.texture = TIMING_BRICK_TEXTURES[v]
		E.BrickType.FAST:
			Texture_.texture = FAST_BRICK_TEXTURES[v]
		E.BrickType.SWELL:
			Texture_.texture = SWELL_BRICK_TEXTURES[v]
		_:
			Texture_.texture = bricks_texture[type]
	
func clear_children(node: Node):
	for child in node.get_children():
		child.queue_free()
		
func control():
	var is_problem := false
	var gold_brick_count := 0
	for b: Area2D in Bricks.get_children():
		var has_overlapping := len(b.get_overlapping_areas()) || len(b.get_overlapping_bodies())
		is_problem = is_problem || has_overlapping
		b.get_node("Error").visible = has_overlapping
		if b.get_meta("type") == E.BrickType.GOLD:
			gold_brick_count += 1
	if gold_brick_count < 2 * level.gold_to_win - 1:
		Error.visible = true
		ErrorTimer.start()
		ErrorLabel.text = "Impossible de tester le niveau : il manque " + str(2 * level.gold_to_win - 1 - gold_brick_count) + " or(s)\n( 2 * nombre_or - 1 )"
		return true
	if is_problem:
		Error.visible = true
		ErrorTimer.start()
		ErrorLabel.text = "Impossible de tester le niveau : certaines briques se chevauchent ou sont à des emplacements interdits"
		return true
	return false

func change_symmetry():
	match level.symmetry:
		E.SymmetryOption.H:
			MouseBrick0.set_meta("active", true)
			MouseBrick1.set_meta("active", true)
			MouseBrick2.set_meta("active", false)
			MouseBrick3.set_meta("active", false)
		E.SymmetryOption.HV:
			MouseBrick0.set_meta("active", true)
			MouseBrick1.set_meta("active", true)
			MouseBrick2.set_meta("active", true)
			MouseBrick3.set_meta("active", true)
		E.SymmetryOption.CENTRAL:
			MouseBrick0.set_meta("active", true)
			MouseBrick1.set_meta("active", false)
			MouseBrick2.set_meta("active", false)
			MouseBrick3.set_meta("active", true)
	Symmetry.icon = load("res://src/assets/sprites/ui/Symmetry{0}.png".format([level.symmetry + 1]))

func _brick_chosen(index: int):
	add_variant(index, 0)
	current_brick_type = index as E.BrickType
	
func _on_more_options_toggled(toggled_on: bool):
	Player.play("OptionScroll", -1, 1 if toggled_on else -1, !toggled_on)

func _on_name_text_changed(new_name: String):
	level.name = new_name

func _on_symmetry_pressed():
	if level.validated:
		EditValidated.visible = true
		return
	level.symmetry = (level.symmetry + 1) % E.SymmetryOption.LENGTH as E.SymmetryOption
	clear_children(Bricks)
	build_level()

func _on_gold_to_win_value_changed(new_treshold: float):
	if level.validated:
		GoldToWin.value = level.gold_to_win
		EditValidated.visible = true
		return
	level.gold_to_win = int(new_treshold)

func _on_grid_snap_value_changed(new_snap: float, axis: String):
	level.snap[axis] = int(new_snap)
	_draw_grid()

func _on_grid_offset_value_changed(new_offset: float, axis: String):
	level.offset[axis] = int(new_offset)
	_draw_grid()

func _draw_grid():
	Grid.offset = 8 * level.offset
	Grid.snap = 8 * level.snap
	Grid.queue_redraw()

func _on_rotate_pressed():
	brick_rotation = (brick_rotation + 90) % 180
	for MB in MouseBricks.get_children():
		MB.rotation_degrees = brick_rotation

func _on_change_state_pressed():
	var VariantEnum : Dictionary = C.VariantEnumByType[current_brick_type] if C.VariantEnumByType.has(current_brick_type) else {"LENGTH": 1}
	add_variant(current_brick_type, (current_brick_variant + 1) % VariantEnum.LENGTH)

func _on_play_pressed():
	if control():
		return
	State.current_level = -1
	State.mode = E.Mode.TESTING
	State.ai_level = [0, 0]
	State.total_score = [0, 0]
	get_tree().change_scene_to_file("res://src/scenes/arena/Arena.tscn")

func _on_export_pressed():
	var save := level.save()
	if len(State.custom_levels) <= State.custom_level_index:
		State.custom_levels.append(State.level)
	else:
		State.custom_levels[State.custom_level_index] = State.level
	State.save_data()
	Export.visible = true
	Save.text = save
	ExportError.visible = !level.validated

func _on_import_pressed():
	Import.visible = true
	PasteArea.text = ""
	ImportError.visible = false

func _on_copy_pressed():
	DisplayServer.clipboard_set(Save.text)
	Export.visible = false
	
func _on_load_pressed():
	var old_level := level
	var new_level := LevelData.load_(PasteArea.text)
	if new_level:
		level = new_level
		State.level = new_level
		clear_children(Bricks)
		build_level()
		Import.visible = false
		return
	ImportError.visible = true
	level = old_level
	State.level = old_level

func _on_back_pressed():
	Export.visible = false
	Import.visible = false
	EditValidated.visible = false
	Exit.visible = false

func _on_yes_pressed():
	level.validated = false
	EditValidated.visible = false

func _on_error_timer_timeout():
	Error.visible = false
	
func _on_exit_pressed():
	Exit.visible = true
	
func _on_exit_yes_pressed():
	get_tree().change_scene_to_file("res://src/scenes/menus/EditorMenu.tscn")

func _input(event: InputEvent) -> void:
	if Controls.visible:
		if !(event is InputEventMouse) and event.is_pressed():
			if !controls_page:
				ControlsPage1.visible = false
				ControlsPage2.visible = true
				MoreOptions.button_pressed = true
				controls_page += 1
			else:
				Controls.hide.call_deferred()
				State.have_seen_editor_controls = true
				MoreOptions.button_pressed = false
				Help.grab_focus()
		get_viewport().set_input_as_handled()

func _on_help_pressed():
	Help.release_focus()
	MoreOptions.button_pressed = false
	Controls.visible = true
	ControlsPage1.visible = true
	ControlsPage2.visible = false
	controls_page = 0
