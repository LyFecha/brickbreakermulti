extends Control

@export var State: StateType

const LevelButton : PackedScene = preload("res://src/scenes/commons/LevelButton.tscn")

@onready var Levels : GridContainer = $Panel/Levels
@onready var Buttons : HBoxContainer = $Panel/Buttons
@onready var ImportButton : Button = $Panel/Buttons/Import
@onready var ExportButton : Button = $Panel/Buttons/Export
@onready var Delete : Button = $Panel/Buttons/Delete
@onready var Exit : Button = $Panel/Buttons/Exit
@onready var Import : Panel = $Import
@onready var ImportWarning : Label = $Import/Warning
@onready var PasteArea : TextEdit = $Import/PasteArea
@onready var ImportError : Label = $Import/Error
@onready var Export : Panel = $Export
@onready var Save : TextEdit = $Export/Save
@onready var ExportError : Label = $Export/Error
@onready var Edit : Button = $Panel/Buttons/Edit

var selected_level : int = -1

func _load(path: String) -> ImageTexture:
	var image = Image.new()
	var err = image.load(path)
	if(err != 0):
		print("Error loading the image at: " + path)
		return null
	return ImageTexture.create_from_image(image)

func _ready():
	ExportButton.disabled = true
	Edit.disabled = true
	Delete.disabled = true
	for i in range(len(State.custom_levels)):
		var level := State.custom_levels[i]
		var lb := LevelButton.instantiate() as LevelButtonClass
		Levels.add_child(lb)
		Levels.move_child(lb, Levels.get_child_count() - 2)
		lb.label.text = level.name if level.name else "<Sans nom>"
		if level.preview_path:
			lb.preview.texture = _load(level.preview_path)
		lb.tooltip_text = level.name
		lb.pressed.connect(_on_level_presssed.bind(i))

func _on_level_presssed(id: int):
	selected_level = id
	ExportButton.disabled = false
	Edit.disabled = false
	Delete.disabled = false

func _on_new_level_pressed():
	var ok_button : Button = Confirm.display_dialog("Voulez-vous créer un nouveau niveau ?")
	State.level = LevelData.new()
	State.custom_level_index = len(State.custom_levels)
	ok_button.pressed.connect(_on_confirm_pressed)
	
func _on_confirm_pressed():
	get_tree().change_scene_to_file("res://src/scenes/editor/Editor.tscn")

func _on_import_pressed():
	Import.visible = true
	ImportWarning.visible = false
	ImportError.visible = false

func _on_export_pressed():
	var exported_level := State.custom_levels[selected_level]
	Save.text = exported_level.save()
	Export.visible = true
	ExportError.visible = !exported_level.validated

func _on_edit_pressed():
	State.level = State.custom_levels[selected_level]
	State.custom_level_index = selected_level
	get_tree().change_scene_to_file("res://src/scenes/editor/Editor.tscn")

func _on_load_pressed():
	var new_level := LevelData.load_(PasteArea.text)
	if !new_level:
		ImportError.visible = true
		return
	State.custom_levels.append(new_level)
	State.save_data()
	var lb := LevelButton.instantiate() as Button
	var i := Levels.get_child_count() - 1
	Levels.add_child(lb)
	Levels.move_child(lb, i)
	lb.get_node("Label").text = new_level.name if new_level.name else "<Sans nom>"
	lb.pressed.connect(_on_level_presssed.bind(i))
	Import.visible = false

func _on_back_pressed():
	Import.visible = false
	Export.visible = false

func _on_copy_pressed():
	DisplayServer.clipboard_set(Save.text)
	Export.visible = false

func _on_delete_pressed():
	var ok_button : Button = Confirm.display_dialog("Etes-vous sûr de vouloir supprimer ce niveau ?")
	ok_button.pressed.connect(_on_confirm_delete_pressed)

func _on_confirm_delete_pressed():
	State.custom_levels.remove_at(selected_level)
	State.save_data()
	Levels.remove_child(Levels.get_child(selected_level))
	selected_level = -1
	Delete.disabled = true
	ExportButton.disabled = true
	Edit.disabled = true

func _on_exit_pressed():
	get_tree().change_scene_to_file("res://src/scenes/menus/MainMenu.tscn")
