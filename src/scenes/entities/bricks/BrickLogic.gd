extends RigidBody2D
class_name BrickLogic

@export var State: StateType

@export var points_type : E.PointType = E.PointType.BRICK 
@export var brick_points : int = 20
@export var brick_effect : E.BrickEffect = E.BrickEffect.NONE
@export var brick_hp : int = 1
@export var brick_sound : E.BrickSound = E.BrickSound.NONE
@export var sprite_node : Polygon2D

signal points(player_id: int, type: E.PointType, amount: int)
signal gold(player_id: int)
signal effect(ball: Ball, type: E.BrickEffect)
signal toggled_visible(is_visible: bool)
signal new_hp(hp: int)

@onready var sound: AudioStreamPlayer2D = $Sound
@onready var attenuated_sound: AudioStreamPlayer2D = $AttenuatedSound
@onready var fragments: CPUParticles2D = $KeepVisible/Fragments
@onready var points_wrapper: Control = $KeepVisible/PointsWrapper
@onready var points_label: Label = $KeepVisible/PointsWrapper/Points
@onready var points_player: AnimationPlayer = $PointsPlayer

@onready var hp : int = brick_hp :
	set(new):
		hp = new
		if new > 0:
			fragments.texture = sprite_node.texture
			new_hp.emit(hp)

func _ready() -> void:
	fragments.texture = sprite_node.texture
	points_label.hide()

func _on_body_entered(body: Ball):
	Sound.play_sound(brick_sound, sound, attenuated_sound, body.follow_beam, body.player_id)
	var do_break := collided(body)
	if do_break:
		toggle_visible(false)

func collided(body: Ball) -> bool :
	points.emit(body.player_id, points_type, brick_points)
	if points_type == E.PointType.GOLD:
		gold.emit(body.player_id)
	if brick_effect:
		effect.emit(body, brick_effect)
	if hp > 0:
		hp -= 1
		fragments.emitting = true
		if State.show_points:
			show_points(body.player_id)
	return hp == 0

func toggle_visible(is_visible_: bool):
	get_parent().visible = is_visible_
	set_collision_layer_value(3, is_visible_)
	if is_visible_:
		hp = brick_hp
	toggled_visible.emit(is_visible_)

func show_points(player_id: int) -> void:
	points_label.text = str(brick_points)
	points_wrapper.global_position = global_position
	points_wrapper.modulate = C.COLORS[player_id]
	points_player.play("Points")
