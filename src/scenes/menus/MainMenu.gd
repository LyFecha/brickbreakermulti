extends Control

@export var State: StateType

@onready var Description : Label = $Description
@onready var Match : Button = $Match
@onready var HowToPlay : Button = $HowToPlay
@onready var HowToPlayPanel : Panel = $HowToPlayPanel
@onready var PageCount : Label = $HowToPlayPanel/PageCount
@onready var Next : Button = $HowToPlayPanel/Next
@onready var Exit: Button = $HowToPlayPanel/Exit
@onready var Controls : Control = $HowToPlayPanel/Page4/Controls
@onready var Tournament: Panel = $Tournament


var current_htp_page := 0

func _ready():
	State.load_data()
	Description.visible = false
	Tournament.visible = false
	Exit.visible = State.have_seen_how_to_play
	if !State.have_seen_how_to_play:
		Exit.visible = false
		return _on_how_to_play_pressed()
	Match.grab_focus()

func _on_duel_pressed():
	State.mode = E.Mode.DUEL
	get_tree().change_scene_to_file("res://src/scenes/menus/LevelsMenu.tscn")

func _on_match_pressed():
	State.mode = E.Mode.MATCH
	get_tree().change_scene_to_file("res://src/scenes/menus/LevelsMenu.tscn")

func _on_level_editor_pressed():
	get_tree().change_scene_to_file("res://src/scenes/menus/EditorMenu.tscn")

func _on_options_pressed():
	get_tree().change_scene_to_file("res://src/scenes/menus/OptionsMenu.tscn")
	
func _on_quit_pressed():
	get_tree().quit()



func _on_versus_focus_entered():
	Description.text = "Un match 1 contre 1 sur plusieurs niveaux"
	Description.visible = true
	
func _on_duel_focus_entered():
	Description.text = "Un simple duel 1 contre 1 sur un niveau"
	Description.visible = true

func _on_level_editor_focus_entered():
	Description.text = "Pour créer, importer et exporter des niveaux fait maison"
	Description.visible = true

func _on_options_focus_entered():
	Description.text = "Pour changer les contrôles et autres comportements en jeu"
	Description.visible = true

func _on_how_to_play_focus_entered():
	Description.text = "Explications sur le but du jeu"
	Description.visible = true
	
func _on_quit_focus_entered():
	Description.text = ":("
	Description.visible = true

func _on_focus_exited():
	Description.visible = false

func _on_how_to_play_pressed():
	HowToPlayPanel.visible = true
	Next.grab_focus()
	show_page(0)

func show_page(page_index: int):
	for i in range(6):
		HowToPlayPanel.get_child(i).visible = i == page_index
		
	Controls.process_mode = Node.PROCESS_MODE_ALWAYS if page_index == 4 else Node.PROCESS_MODE_DISABLED
	PageCount.text = str(page_index + 1) + " / 6"
	current_htp_page = page_index

func _on_how_to_play_back_pressed():
	show_page(max(0, current_htp_page - 1))
	
func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("pause1") and State.have_seen_how_to_play:
		_on_exit_pressed()
	
func _on_how_to_play_panel_gui_input(event: InputEvent):
	if !(event is InputEventMouse) and event.is_pressed():
		_on_how_to_play_next()
	
func _on_how_to_play_next():
	if current_htp_page == 5:
		if !State.have_seen_how_to_play:
			State.have_seen_how_to_play = true
			State.save_data()
			Exit.visible = true
		HowToPlayPanel.visible = false
		HowToPlay.grab_focus()
	else:
		show_page(current_htp_page + 1)

func _on_show_tournament_pressed() -> void:
	Tournament.visible = true

func _on_hide_tournament_pressed() -> void:
	Tournament.visible = false

func _on_exit_pressed() -> void:
	HowToPlayPanel.visible = false
	
