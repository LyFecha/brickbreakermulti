extends Button
class_name LevelButtonClass

@onready var label: Label = $Label
@onready var preview: TextureRect = $Preview
@onready var checkbox: TextureRect = $Checkbox
@onready var check : TextureRect = $Check

func _on_toggled(toggled_on: bool):
	check.visible = toggled_on
