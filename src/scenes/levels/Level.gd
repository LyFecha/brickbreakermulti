extends Node2D
class_name Level

const BALL_SCENE : PackedScene = preload("res://src/scenes/entities/Ball.tscn")
const BAR_SCENE : PackedScene = preload("res://src/scenes/entities/bar/Bar.tscn")

const SPEED_HANDICAP_OFFSET := 150.0
const GOAL_OFFSET := 50

@onready var Walls : WallsClass = $Walls

@export var name_ : String
@export var number_of_gold_to_win := 8
@export var size := Vector2(1920, 1000)
@export var background : Texture2D = preload("res://src/assets/textures/backgrounds/BG5.png")

var ai_level : Array[int] = [0, 0]
var is_arrow_visible : bool = true
var show_points : bool = true
var handicap : Array[Dictionary] = [
	{"speed": 0, "size": 0, "gold": 0},
	{"speed": 0, "size": 0, "gold": 0},
]
var balls : Array[Array]
var bars : Array[Bar]
var ball_target_speed := 750.0
var goals := [0, 0]

signal points(player_id: int, type: E.PointType, amount: int)
signal gold(player_id: int)

func _ready():
	var bar1 := BAR_SCENE.instantiate() as Bar
	var bar2 := BAR_SCENE.instantiate() as Bar
	var ball1 := BALL_SCENE.instantiate() as Ball
	var ball2 := BALL_SCENE.instantiate() as Ball
	
	bar2.player_id = 1
	
	bar1.ai_level = ai_level[0]
	bar1.is_arrow_visible = is_arrow_visible
	add_child(bar1)
	bar1.global_position = $Spawns.get_children()[0].global_position
	bar1.set_handicap_scale(handicap[0].size)
	
	bar2.ai_level = ai_level[1]
	bar2.is_arrow_visible = is_arrow_visible
	add_child(bar2)
	bar2.global_position = $Spawns.get_children()[1].global_position
	bar2.set_handicap_scale(handicap[1].size)
	
	bars = [bar1, bar2]
	balls = [[ball1], [ball2]]
	ball2.player_id = 1
	
	bar1.attach_ball(ball1)
	ball1.bar = bar1
	bar2.attach_ball(ball2)
	ball2.bar = bar2
	
	Walls.change_name(name_)

func _physics_process(delta: float):
	var balls_to_remove : Array[Array] = []
	for player_balls_index: int in range(len(balls)):
		for ball_index: int in range(len(balls[player_balls_index])):
			var ball : Ball = balls[player_balls_index][ball_index]
			var ball_x_position := ball.global_position.x
			if ball_x_position < -GOAL_OFFSET or ball_x_position > size.x + GOAL_OFFSET:
				if ball_x_position < -GOAL_OFFSET and ball.player_id == 0 or ball_x_position > size.x + GOAL_OFFSET and ball.player_id == 1:
					if len(balls[ball.player_id]) == 1:
						points.emit(ball.player_id, E.PointType.GOAL, -50)
						Walls.goal(ball.player_id, ball.player_id)
				elif ball_x_position < -GOAL_OFFSET and ball.player_id == 1 or ball_x_position > size.x + GOAL_OFFSET and ball.player_id == 0:
					if goals[ball.player_id] < 5:
						points.emit(ball.player_id, E.PointType.GOAL, 50)
						goals[ball.player_id] += 1
						var other_player_id := (ball.player_id + 1) % 2
						Walls.goal(other_player_id, ball.player_id, goals[ball.player_id] - 1)
				if len(balls[player_balls_index]) - len(balls_to_remove) == 1:
					var new_ball : Ball = BALL_SCENE.instantiate()
					var bar : Bar = bars[player_balls_index]
					balls[player_balls_index][ball_index] = new_ball
					new_ball.linear_velocity = Vector2.ZERO
					new_ball.name = ball.name
					new_ball.player_id = ball.player_id
					new_ball.bar = ball.bar
					new_ball.follow_beam = ball.follow_beam
					for child in ball.get_node("Keys").get_children():
						child.get_parent().remove_child(child)
						new_ball.get_node("Keys").add_child(child)
					new_ball.appear()
					bar.attach_ball(new_ball)
					ball.die(ball_x_position > size.x + -GOAL_OFFSET)
				else:
					balls_to_remove.push_front([player_balls_index, ball_index])
				bars[player_balls_index].speed_reset(ball.speed_modifier)
	for to_remove: Array[int] in balls_to_remove:
		balls[to_remove[0]].remove_at(to_remove[1])
		
	ball_target_speed = clampf(ball_target_speed + delta * 3, 750.0, 1000.0)
	for player_id in range(len(bars)):
		var bar := bars[player_id]
		bar.ball_target_speed = ball_target_speed + handicap[player_id].speed * SPEED_HANDICAP_OFFSET
	for player_id in range(len(balls)):
		for ball in balls[player_id]:
			ball.target_speed = ball_target_speed + handicap[player_id].speed * SPEED_HANDICAP_OFFSET

func _on_points(player_id: int, type: E.PointType, amount: int):
	points.emit(player_id, type, amount)

func _on_gold(player_id: int):
	gold.emit(player_id)
	
func _on_effect(ball: Ball, type: E.BrickEffect):
	$BrickEffect.do_effect(type, ball, self)

func options_update(State: StateType):
	show_points = State.show_points
	Walls.show_points = State.show_points
	for bar in bars:
		bar.options_update(State)
