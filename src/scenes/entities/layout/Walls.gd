extends StaticBody2D
class_name WallsClass

@export var show_points := true

@onready var name_label: Label = $Name

func change_name(name_: String) -> void:
	name_label.text = name_
	
func goal(goal_id: int, points_id: int, goal_number: int = -1) -> void:
	var true_goal := goal_number != -1
	get_node("Goal{0}/Animations".format([goal_id])).play("Goal")
	if show_points:
		get_node("Points{0}".format([points_id])).text = str(50 if true_goal else -50)
		get_node("Points{0}/Animations".format([points_id])).play("Points")
	if goal_number != -1:
		var bulbs := get_node("Bulbs{0}".format([points_id]))
		bulbs.get_child(goal_number).goal(points_id)

func invincibility(player_id: int) -> void:
	var Animations : AnimationPlayer = get_node("Invincibility{0}/Animations".format([player_id]))
	if Animations.is_playing():
		Animations.advance.call_deferred(0.5 - Animations.current_animation_position)
	else:
		Animations.play("Invincibility")
