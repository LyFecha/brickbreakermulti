extends RigidBody2D
class_name Ball

const HORIZONTAL_SPEED_TRESHOLD := 125.0
const KEY_SCENE : PackedScene = preload("res://src/scenes/entities/Key.tscn")

@export var player_id : int = 0 :
	set(id):
		set_color(C.COLORS[id])
		set_collision_mask_value(4 + id, true)
		set_collision_mask_value(5 - id, false)
		player_id = id
@export var attached := true
@export var appearing := false
@export var target_speed := 700.0
@export var sticky := false
@export var speed_modifier := 1.0
@export var current_scale_multiplier := 1.0
@export var base_radius := 13.0
@export var bar : Bar
@export var follow_beam := false
@export var dead := false

@onready var Trail: CPUParticles2D = $Trail
@onready var Sprites: Node2D = $Sprites
@onready var BallSprite: MeshInstance2D = $Sprites/Ball
@onready var Cog: MeshInstance2D = $Sprites/Cog
@onready var Screw: MeshInstance2D = $Sprites/Screw
@onready var Collision: CollisionShape2D = $Collision
@onready var Keys: Node2D = $Keys
@onready var Animations : AnimationPlayer = $AnimationPlayer
@onready var AestethicAnimations : AnimationPlayer = $AestethicAnimations
@onready var StickyTimer : Timer = $StickyTimer
@onready var SpeedChangeTimer : Timer = $SpeedChangeTimer
@onready var ScaleChangeTimer : Timer = $ScaleChangeTimer
@onready var JustLeftBarTimer : Timer = $JustLeftBarTimer
@onready var Sound : AudioStreamPlayer2D = $Sound
@onready var death: CPUParticles2D = $Death
@onready var appearance: CPUParticles2D = $Appearance
@onready var appearance_player: AnimationPlayer = $AppearancePlayer


signal speed_reset(speed_modifier: float)
signal finished_appearing

func _ready() -> void:
	_reset_scale()

func _physics_process(delta: float):
	if dead:
		linear_velocity = Vector2.ZERO
		return
	if !attached:
		linear_velocity = speed_modifier * ((1.0 - delta * 2.0) * target_speed + delta * 2.0 * linear_velocity.length()) * linear_velocity.normalized()
		if abs(linear_velocity.x) < HORIZONTAL_SPEED_TRESHOLD:
			linear_velocity.x += sign(linear_velocity.x) * (delta + sqrt(abs(linear_velocity.x))) / 25
		if follow_beam:
			var speed := linear_velocity.length()
			linear_velocity = Vector2(linear_velocity.x, clamp((bar.global_position.y - global_position.y) * target_speed * delta, - speed * 7 / 8, speed * 7 / 8)).normalized() * speed
	else:
		linear_velocity = -position * scale.x

func _on_body_entered(body: Node):
	if (body is Bar and !attached) or body is Ball:
		Sound.pitch_scale = 0.9 + randf() * 0.2 + (speed_modifier - 1) / 2
		Sound.play()
		if body is Bar and !body.has_attached_ball() and sticky and player_id == body.player_id:
			var bar_body = body as Bar
			linear_velocity = Vector2.ZERO
			bar_body.set_path_point_closest_to(self)
			bar_body.attach_ball(self)

func set_color(color: Color):
	$Sprites/Screw.modulate = color
	$Trail.modulate = color
	$Collision.debug_color = Color(color, 0.3)
	$Death.color = color
	$Appearance.color = color

func do_collision_with_bar(value: bool):
	set_collision_mask_value(2, value)

func unattached():
	attached = false
	mass = 1.0
	do_collision_with_bar(false)
	JustLeftBarTimer.start()

func stick():
	sticky = true
	if Animations.is_playing() && Animations.current_animation == "StickyFade":
		Animations.stop()
	BallSprite.modulate = Color("#00ff00")
	Cog.modulate = Color("#00ff00")
	StickyTimer.start()
	
func _sticky_timeout():
	Animations.play("StickyFade")
	
func change_speed(new_speed: float):
	if Animations.is_playing() && Animations.current_animation == "FastFade":
		Animations.stop()
	if speed_modifier * new_speed < 2.5 and speed_modifier * new_speed > 0.4: 
		speed_modifier *= new_speed
		BallSprite.self_modulate = Color(speed_modifier, speed_modifier, speed_modifier, 1)
		Cog.self_modulate = Color(speed_modifier, speed_modifier, speed_modifier, 1)
	SpeedChangeTimer.start()
	
func change_scale(new_scale: float):
	if AestethicAnimations.is_playing() && AestethicAnimations.current_animation == "BlinkScale":
		AestethicAnimations.play("RESET")
	if current_scale_multiplier + new_scale < 5 and current_scale_multiplier + new_scale > 0.1:
		(Collision.shape as CircleShape2D).radius = base_radius * (current_scale_multiplier + new_scale)
		Sprites.apply_scale(Vector2.ONE * (current_scale_multiplier + new_scale) / current_scale_multiplier)
		current_scale_multiplier += new_scale
	ScaleChangeTimer.start()
	
func _scale_change_timeout():
	AestethicAnimations.play("BlinkScale")
	
func _reset_scale():
	current_scale_multiplier = 1
	(Collision.shape as CircleShape2D).radius = base_radius
	Sprites.scale = Vector2.ONE
	scale = Vector2.ONE
	
func _reset_speed():
	speed_reset.emit(speed_modifier)
	var fast_fade := Animations.get_animation("FastFade")
	for i in range(7):
		var mult := (2 if !(i % 2) && i != 7 else 1)
		fast_fade.track_set_key_value(0, i, BallSprite.self_modulate * mult)
		fast_fade.track_set_key_value(1, i, Cog.self_modulate * mult)
	fast_fade.track_set_key_value(2, 0, speed_modifier)
	Animations.play("FastFade")
	
func _left_bar_timeout():
	do_collision_with_bar(true)
	
func add_key():
	var key := KEY_SCENE.instantiate() as Node2D
	Keys.add_child(key)
	key.position = Vector2(30, 0).rotated(randf() * 2 * PI)

func remove_key_or_nothing() -> bool:
	if Keys.get_child_count():
		var key := Keys.get_child(0)
		key.queue_free()
		Keys.remove_child(key)
		return true
	return false

func die(right_goal: bool) -> void:
	linear_velocity = Vector2.ZERO
	dead = true
	if right_goal:
		death.direction = Vector2(-1, 0)
	death.emitting = true

func _on_death_finished() -> void:
	queue_free()

func appear() -> void:
	appearing = true
	$AppearancePlayer.play("Appearance")
	
func appear_timeout() -> void:
	appearing = false
	finished_appearing.emit()
