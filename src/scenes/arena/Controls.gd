extends Panel

signal undisplayed

@onready var Progress : Line2D = $Controls/Continue/Progress

var progress : float = 0

func _process(delta: float):
	if visible and (Input.is_action_pressed("pause1") or Input.is_action_pressed("pause2") or Input.is_action_pressed("accept1") or Input.is_action_pressed("accept2") or Input.is_action_pressed("ui_accept") or (Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT))):
		progress += 2 * delta / (4 if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) else 1)
		if progress >= 1:
			undisplay.call_deferred()
			get_viewport().set_input_as_handled()
	elif progress > 0:
		progress = clampf(progress - 2 * delta, 0, 1)
	Progress.set_point_position(1, Vector2(832 + progress * (1088 - 832), 560))


func undisplay():
	visible = false
	undisplayed.emit()
