extends Node2D

func _ready():
	var polygons_to_remove: Array
	while(true):
		polygons_to_remove = []
		for child_index in get_parent().get_child_count():
			var child := get_parent().get_child(child_index) as Node2D
			if child == null:
				continue
			var found_polygon := child.get_node_or_null("BrickLogic/CollisionPolygon2D") as CollisionPolygon2D
			if found_polygon == null or found_polygon.is_queued_for_deletion():
				continue

			if child.transform * found_polygon.transform != Transform2D.IDENTITY:
				var transformed_polygon := child.transform * found_polygon.polygon
				found_polygon.transform = child.transform.affine_inverse()
				found_polygon.polygon = PackedVector2Array(Array(transformed_polygon).map(func(v): return round(v)))

			for child_subindex in child_index:
				var other_child := get_parent().get_child(child_subindex) as Node2D
				if other_child == null:
					continue
				var other_found_polygon := other_child.get_node_or_null("BrickLogic/CollisionPolygon2D") as CollisionPolygon2D
				if other_found_polygon == null or other_found_polygon.is_queued_for_deletion():
					continue

				var merged_polygon := Geometry2D.merge_polygons(found_polygon.polygon, other_found_polygon.polygon)
				if merged_polygon.size() != 1:
					continue

				other_found_polygon.polygon = merged_polygon[0]
				polygons_to_remove.append(found_polygon)
				break

		if polygons_to_remove.size() == 0:
			break

		for polygon_to_remove in polygons_to_remove:
			polygon_to_remove.queue_free()
