extends Control
class_name GridClass

const MIN_POSITION := Vector2(0, 144)
const MAX_POSITION := Vector2(1920, 1016)
const CENTER := (MAX_POSITION + MIN_POSITION) / 2

@export var offset := Vector2(0, 0)
@export var snap := Vector2(32, 32)

func _draw():
	var offset_x := offset.x + int(CENTER.x) % int(snap.x)
	var offset_y := offset.y + int(CENTER.y) % int(snap.y) - 4
	while offset_x < size.x:
		draw_line(Vector2(offset_x, 0), Vector2(offset_x, size.y), Color(0.8, 0.8, 0.8, 0.5), 1)
		offset_x += snap.x
	while offset_y < size.y:
		draw_line(Vector2(0, offset_y), Vector2(size.x, offset_y), Color(0.8, 0.8, 0.8, 0.5), 1)
		offset_y += snap.y
