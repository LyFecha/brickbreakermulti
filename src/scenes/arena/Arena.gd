extends Control

const MIN_POSITION := Vector2(0,64)
const MAX_POSITION := Vector2(1920, 936)
const CENTER := (MAX_POSITION + MIN_POSITION) / 2
const SCREENSHOT_REGION := Rect2i(240, 160, 1440, 872)

@export var State: StateType

@onready var Animations: AnimationPlayer = $AnimationPlayer
@onready var ArenaUI: ArenaUIClass = $ArenaUI
@onready var Countdown: Control = $Countdown
@onready var CountdownAnimation: AnimationPlayer = $Countdown/CountdownPlayer
@onready var LevelContainer: SubViewportContainer = $Level/Container
@onready var Viewport_: SubViewport = $Level/Container/Viewport
@onready var ScoreGrid: GridContainer = $Score/Grid
@onready var ScoreGrid5: Label = $Score/Grid/Label5
@onready var Victory1: Node2D = $Score/Victory1
@onready var GoldPile1: Sprite2D = $Score/GoldPile1
@onready var Victory2: Node2D = $Score/Victory2
@onready var GoldPile2: Sprite2D = $Score/GoldPile2
@onready var Pause : Panel = $Pause
@onready var PauseButton : TextureButton = $PauseButton

const ID_TO_POS := ["Left", "Right"]
const VICTORY_X_POS := [320, 1600]

var winner := -1

func _ready():
	if Engine.is_editor_hint():
		State.current_level = 0
	get_tree().paused = true
	var level : Level 
	if State.mode == E.Mode.TESTING:
		level = create_level()
	elif State.current_level >= State.BASE_LEVEL_MAX:
		State.level = State.custom_levels[State.current_level - State.BASE_LEVEL_MAX]
		level = create_level()
	else:
		level = load("res://src/scenes/levels/Level{0}.tscn".format([State.current_level + 1])).instantiate()
		level.name_ = C.LEVEL_NAMES[State.current_level]
	set_level(level)
	Countdown.visible = false
	Victory1.visible = false
	GoldPile1.visible = false
	Victory2.visible = false
	GoldPile2.visible = false
	Pause.visible = false
	PauseButton.visible = true
	# Enable for level screenshot !
	if State.mode == E.Mode.TESTING:
		await RenderingServer.frame_post_draw
		var time := Time.get_datetime_string_from_system(true).replacen(":", "")
		var screenshot = get_viewport().get_texture().get_image().get_region(SCREENSHOT_REGION)
		screenshot.resize(320, 194)
		State.level.preview_path = "user://level{0}.png".format([time])
		screenshot.save_png(State.level.preview_path)
	if !Engine.is_editor_hint() && !State.have_seen_controls: 
		$Controls.visible = true
		State.have_seen_controls = true
	else:
		$Controls.visible = false
		_on_controls_undisplayed()

func create_level():
	var level : Level = load("res://src/scenes/levels/BaseLevel.tscn").instantiate()
	level.number_of_gold_to_win = State.level.gold_to_win
	level.name_ = State.level.name
	for b in State.level.brick_data:
		var x_prime = 2 * CENTER.x - b.position.x
		var y_prime = 2 * CENTER.y - b.position.y
		var bricks := [b] as Array[BrickData]
		if (State.level.symmetry == E.SymmetryOption.H or State.level.symmetry == E.SymmetryOption.HV) and x_prime != b.position.x:
			bricks.append(BrickData.new(b.type, State.calculate_symmetry_variant(b.type, b.variant, E.SymmetryOption.H), Vector2(x_prime, b.position.y), b.rotation))
			
			if (State.level.symmetry == E.SymmetryOption.HV) and y_prime != b.position.y:
				bricks.append(BrickData.new(b.type, State.calculate_symmetry_variant(b.type, b.variant, E.SymmetryOption.HV), Vector2(b.position.x, y_prime), b.rotation))
		
		if (State.level.symmetry == E.SymmetryOption.CENTRAL or State.level.symmetry == E.SymmetryOption.HV) and (x_prime != b.position.x or y_prime != b.position.y):
			bricks.append(BrickData.new(b.type, State.calculate_symmetry_variant(b.type, b.variant, E.SymmetryOption.CENTRAL), Vector2(x_prime, y_prime), b.rotation))
		
		for brick in bricks:
			var b_node := load("res://src/scenes/entities/bricks/{0}.tscn".format([C.BrickByType[brick.type]])).instantiate() as Brick
			var variant_node : Brick = b_node.set_variant(brick.variant)
			if variant_node:
				b_node = variant_node
				b_node.set_variant(brick.variant)
			level.add_child(b_node)
			b_node.effect.connect(level._on_effect)
			b_node.gold.connect(level._on_gold)
			b_node.points.connect(level._on_points)
			b_node.position = brick.position
			b_node.rotation_degrees = brick.rotation
	return level
	

func set_level(level: Level):
	if Viewport_.get_child_count() > 0:
		Viewport_.remove_child(Viewport_.get_child(0))
	level.name = "Level"
	level.points.connect(_on_points)
	level.gold.connect(_on_gold)
	ArenaUI.number_of_gold_to_win = level.number_of_gold_to_win
	ArenaUI.gold_handicap = State.handicap.map(func(h) -> int: return h.gold)
	level.ai_level = State.ai_level
	level.is_arrow_visible = State.is_arrow_visible
	level.show_points = State.show_points
	level.handicap = State.handicap
	var background := level.background
	$Background.texture = background
	Viewport_.add_child(level)
	Viewport_.size = level.size
	var scale_float : float = min($Level.size.x / level.size.x, $Level.size.y / level.size.y)
	LevelContainer.scale = Vector2(scale_float, scale_float)
	LevelContainer.queue_sort()

func _unhandled_input(event: InputEvent):
	if winner < 0 and (event.is_action_pressed("pause1") or event.is_action_pressed("pause2")):
		Pause.pause(true)
	elif winner >= 0 and (event.is_action_pressed("ui_accept") or event is InputEventMouseButton):
		if Animations.is_playing():
			var queue := Animations.get_queue()
			Animations.play(Animations.current_animation, -1, 4)
			for queued in queue:
				Animations.queue(queued)
		elif State.mode == E.Mode.TESTING:
			State.level.validated = true
			get_tree().change_scene_to_file("res://src/scenes/editor/Editor.tscn")
		elif State.mode == E.Mode.MATCH and State.level_index < len(State.level_order) - 1:
			State.level_index += 1
			State.current_level = State.level_order[State.level_index]
			get_tree().reload_current_scene()
		else:
			get_tree().change_scene_to_file("res://src/scenes/menus/MainMenu.tscn")
			
func _on_arena_ui_game_end(player_id: int):
	Beep.stop()
	PauseButton.visible = false
	LevelContainer.set_deferred("process_mode", Node.PROCESS_MODE_DISABLED)
	Animations.play("End")
	winner = player_id

func animation_end():
	Animations.play("Score", -1, Animations.speed_scale)
	for player_id: int in [0,1]:
		for score_line: int in range(0,5 if State.mode == E.Mode.DUEL else 6):
			var score_label : ScoreLabel = ScoreGrid.get_node(ID_TO_POS[player_id] + str(score_line + 1))
			if score_line == 5:
				score_label.score = State.total_score[player_id]
			elif score_line == 4:
				var sum : int = ArenaUI.points[player_id].reduce(func(accum, number): return accum + number, 0)
				State.total_score[player_id] += sum
				score_label.score = sum
			else:
				score_label.score = ArenaUI.points[player_id][score_line]
	if State.mode == E.Mode.DUEL:
		ScoreGrid5.text = "TOTAL"
		Animations.queue("Bell")
		setup_victory()
	else:
		Animations.queue("ScoreDuel")
		Animations.queue("Bell")
		ScoreGrid5.text = "Sous-total"
		if State.level_index == len(State.level_order) - 1:
			setup_victory()

func _on_points(player_id: int, type: E.PointType, amount: int):
	ArenaUI.add_points(player_id, type, amount)

func _on_gold(player_id: int):
	ArenaUI.add_gold(player_id)

func _on_controls_undisplayed():
	Countdown.visible = true
	CountdownAnimation.play("Countdown")
	
func unpause():
	get_tree().paused = false

func setup_victory():
	Animations.queue("Victory")
	Victory1.visible = State.total_score[0] >= State.total_score[1]
	GoldPile1.visible = State.total_score[0] >= State.total_score[1]
	Victory2.visible = State.total_score[0] <= State.total_score[1]
	GoldPile2.visible = State.total_score[0] <= State.total_score[1]

func _on_pause_button_pressed():
	if winner < 0:
		Pause.pause(true)

func _on_pause_unpause():
	Viewport_.get_child(0).options_update(State)
