extends Node2D
class_name Brick

@export var rotation_speed := 0.0

@onready var Logic : BrickLogic = $BrickLogic
@onready var Sprite : Polygon2D = $Sprite

signal points(player_id: int, type: E.PointType, amount: int)
signal gold(player_id: int)
signal effect(ball: Ball, type: E.BrickEffect)

func _ready():
	Logic.angular_velocity = rotation_speed / 180 * PI

func _process(delta: float):
	if rotation_speed:
		Sprite.rotation += delta * rotation_speed / 180 * PI

func set_variant(_variant: int):
	pass

func _on_brick_logic_points(player_id: int, type: E.PointType, amount: int):
	points.emit(player_id, type, amount)
	
func _on_brick_logic_gold(player_id: int):
	gold.emit(player_id)
	
func _on_brick_logic_effect(ball: Ball, type: E.BrickEffect):
	effect.emit(ball, type)
