extends Panel

@export var State : StateType

var Options : Control

signal unpause

func pause(do_pause: bool):
	if do_pause:
		($VBoxContainer/Resume as Button).grab_focus()
	get_tree().paused = do_pause
	visible = do_pause
	if !do_pause:
		unpause.emit()

func _on_resume_pressed():
	pause(false)

func _on_quit_pressed():
	pause(false)
	Beep.stop()
	if State.mode == E.Mode.TESTING:
		get_tree().change_scene_to_file("res://src/scenes/editor/Editor.tscn")
		return
	get_tree().change_scene_to_file("res://src/scenes/menus/MainMenu.tscn")

func _on_options_pressed():
	if !Options:
		Options = (load("res://src/scenes/menus/OptionsMenu.tscn") as PackedScene).instantiate()
		add_child(Options)
	Options.visible = true
	Options.change_scene_on_quit = false
