class_name LevelData

var name := ""
var gold_to_win := 8
var validated := false
var preview_path : String

var snap := Vector2(4,4)
var offset := Vector2.ZERO

var symmetry := E.SymmetryOption.H
var brick_data : Array[BrickData] = []

func old_init(name_ := "", gold_to_win_ := 8, validated_ := false, snap_ := 4, offset_ := 0, symmetry_ := E.SymmetryOption.H, brick_data_ : Array[BrickData] = []):
	self.name = name_
	self.gold_to_win = gold_to_win_
	self.validated = validated_
	self.snap = Vector2(snap_, snap_)
	self.offset = Vector2(offset_, offset_)
	self.symmetry = symmetry_
	self.brick_data = brick_data_

func _init(name_ := "", gold_to_win_ := 8, validated_ := false, snap_ := Vector2(4, 4), offset_ := Vector2.ZERO, symmetry_ := E.SymmetryOption.H, brick_data_ : Array[BrickData] = [], preview_path_ : String = ""):
	self.name = name_
	self.gold_to_win = gold_to_win_
	self.validated = validated_
	self.snap = snap_
	self.offset = offset_
	self.symmetry = symmetry_
	self.brick_data = brick_data_
	self.preview_path = preview_path_

func save() -> String:
	return Marshalls.raw_to_base64(var_to_bytes({
		"name": name,
		"gold_to_win": gold_to_win,
		"validated": validated,
		"snap.x": snap.x,
		"snap.y": snap.y,
		"offset.x": offset.x,
		"offset.y": offset.y,
		"symmetry": symmetry,
		"brick_data": brick_data.map(func(bd): return bd.save()),
		"preview_path": preview_path,
	}))

static func load_(s: String, is_snap_vector: bool = true) -> LevelData:
	var unsafe_d = bytes_to_var(Marshalls.base64_to_raw(s))
	if unsafe_d:
		var d := unsafe_d as Dictionary
		var dbd : Array[BrickData] = []
		dbd.assign(d.brick_data.map(func(bd: Dictionary) -> BrickData: return BrickData.load_(bd)))
		if is_snap_vector:
			return LevelData.new(d.name, d.gold_to_win, d.validated, Vector2(d["snap.x"], d["snap.y"]), Vector2(d["offset.x"], d["offset.y"]), d.symmetry, dbd, d.preview_path)
		var new_level := LevelData.new()
		new_level.old_init(d.name, d.gold_to_win, d.validated, d.snap, d.offset, d.symmetry, dbd)
		return new_level
	return null
