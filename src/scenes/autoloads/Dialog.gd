extends AcceptDialog

func _process(_delta: float):
	if visible && get_index() != get_parent().get_child_count() - 1:
		get_parent().move_child(self, get_parent().get_child_count() - 1)
		
func display_dialog(message: String):
	popup_centered_clamped(Vector2(1600,100), 0.9)
	dialog_text = message
	get_ok_button().set_action_mode(BaseButton.ACTION_MODE_BUTTON_RELEASE)
