extends Control
class_name ArenaUIClass

@onready var gold_container : Array[Control] = [$Golds/GoldBar, $Golds/GoldBar2]

@export var points : Array[Array] = [[0,0,0,0,0], [0,0,0,0,0]]
@export var gold_amount : Array[int] = [0, 0]
@export var number_of_gold_to_win : int = 2
@export var gold_handicap : Array = [0, 0]

signal game_end(player_id: int)

func add_gold(player_id: int):
	gold_amount[player_id] += 1
	gold_container[player_id].change_gauge(gold_amount[player_id], number_of_gold_to_win - gold_handicap[player_id])
	if gold_amount[player_id] == number_of_gold_to_win - gold_handicap[player_id]:
		add_points(player_id, E.PointType.END_BONUS, 500)
		game_end.emit(player_id)
	points[player_id][E.PointType.GOLD] += 15 * gold_handicap[player_id] ** 2

func add_points(player_id: int, type: E.PointType, amount: int):
	points[player_id][type] += amount
