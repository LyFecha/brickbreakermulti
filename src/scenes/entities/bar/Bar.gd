extends AnimatableBody2D
class_name Bar

@export var player_id := 0 :
	set(id):
		set_color(C.COLORS[id])
		scale = Vector2(-1, 1) * abs(scale.x)
		base_bar_scale = Vector2(-1, 1)
		player_id = id

const VELOCITY_DECELERATION := 6.0
const VELOCITY_BOOST := 65.0

@onready var UpParticles: CPUParticles2D = $PropulsionParticles/UpParticles
@onready var DownParticles: CPUParticles2D = $PropulsionParticles/DownParticles
@onready var Beam : Sprite2D = $Beam
@onready var Extension1Collision : CollisionPolygon2D = $Extension1
@onready var Extension1 : Sprite2D = $Extension1/Extension
@onready var Extension2Collision : CollisionPolygon2D = $Extension2
@onready var Extension2 : Sprite2D = $Extension2/Extension
@onready var Base : Sprite2D = $BaseCollision/Base
@onready var BaseCollision : CollisionShape2D = $BaseCollision
@onready var Path : Path2D = $Path
@onready var Follow : PathFollow2D = $Path/Follow
@onready var RayCast : RayCast2D = $Path/Follow/RayCast
@onready var BallPoint : Node2D = $Path/Follow/BallPoint
@onready var Arrow : Sprite2D = $Path/Follow/Arrow
@onready var ScaleChangeTimer : Timer = $ScaleChangeTimer
@onready var FirstTimer : Timer = $AI/FirstTimer
@onready var SecondTimer : Timer = $AI/SecondTimer
@onready var StateTimer: Timer = $AI/StateTimer
@onready var BeamAITimer: Timer = $AI/BeamAITimer
@onready var Animations : AnimationPlayer = $Animations
@onready var AestethicAnimations : AnimationPlayer = $AestethicAnimations
@onready var BeamAnimations : AnimationPlayer = $BeamAnimations
@onready var CurseAnimations : AnimationPlayer = $CurseAnimations

@onready var current_scale_multiplier : float = 1.0
@onready var base_base_scale := Base.scale
@onready var base_bar_scale := scale
@onready var handicap_size_offset := 0.0

@export var speed_modifier := 1.0
@export var ball_target_speed := 750.0
@export var ai_level : int = 0
@export var is_arrow_visible : bool = true
var ball_ai : Ball
var initial_direction_ai := 0
var timer_finished_ai := false
var idle_state_ai := false
var direction_beam_ai := -1
var move_beam_ai := false

var is_cursed := false
var velocity := Vector2.ZERO
var extension_animation_speed := 0
var second_extension_on := false
var touch_events := {}
var touch_durations := {}

func _ready():
	Animations.play("ExtensionChange", -1, 0)
	extension_animation_speed = 0
	Follow.progress_ratio = 0.35
	Arrow.visible = false
	UpParticles.emitting = false
	DownParticles.emitting = false
	_reset_scale()

func _input(event: InputEvent):
	if event is InputEventScreenTouch:
		touch_events[event.index] = event
	if event is InputEventScreenDrag:
		if touch_events.has(event.index):
			touch_events[event.index].position = event.position
	
func touch_screen_result(eval: String):
	for index in touch_durations.keys():
		var touch_event := touch_events[index] as InputEventScreenTouch
		var touch_position := touch_event.position
		var touch_duration := touch_durations[index] as float
		
		var in_bounds := touch_position.x > global_position.x - 200\
			and touch_position.x < global_position.x + 200
		if !in_bounds: continue
		match eval:
			"down":
				return touch_event.pressed \
					and touch_position.y > global_position.y + 25
			"up":
				return touch_event.pressed \
					and touch_position.y < global_position.y - 25
			"accept":
				return !touch_event.pressed \
					and touch_duration < 0.2 \
					and has_attached_ball() \
					and touch_position.y > global_position.y - 50 \
					and touch_position.y < global_position.y + 50
			"back":
				return !touch_event.pressed \
					and touch_duration < 0.2 \
					and !has_attached_ball() \
					and touch_position.y > global_position.y - 50 \
					and touch_position.y < global_position.y + 50
			"slow":
				return touch_event.pressed \
					and touch_position.y > global_position.y - 25 \
					and touch_position.y < global_position.y + 25
	return false
	

func _physics_process(delta: float):
	for index in touch_events.keys():
		if touch_durations.has(index):
			touch_durations[index] += delta
		else:
			touch_durations[index] = delta
	var is_down := false
	var is_up := false
	var is_back := false
	var is_accept := false
	var is_slow := false
	if ai_level:
		if ai_level < 3 and StateTimer.is_stopped():
			_on_state_timer_timeout()
		if idle_state_ai:
			UpParticles.emitting = false
			DownParticles.emitting = false
			return
		if has_attached_ball():
			if timer_finished_ai:
				initial_direction_ai = 0
				timer_finished_ai = false
				is_accept = true
			elif !initial_direction_ai:
				initial_direction_ai = -1 if randf() > 0.5 else 1
				FirstTimer.wait_time = randf() / 2.0
				SecondTimer.wait_time = randf() / 2.0
				FirstTimer.start(FirstTimer.wait_time)
			elif initial_direction_ai == -1:
				is_up = true
			elif initial_direction_ai == 1:
				is_down = true
		elif ball_ai.follow_beam:
			if BeamAITimer.is_stopped():
				_on_beam_ai_timer_timeout()
			if global_position.y < 170:
				direction_beam_ai = 1
			elif global_position.y > 830:
				direction_beam_ai = -1
			if move_beam_ai:
				if direction_beam_ai == -1:
					is_up = true
				elif direction_beam_ai == 1:
					is_down = true
		elif !ball_ai.appearing:
			var diff := ball_ai.position.y - position.y
			if diff > 50:
				is_down = true
			elif diff < -50:
				is_up = true
	else:
		is_down = Input.is_action_pressed("down{0}".format([player_id + 1]), true) or touch_screen_result("down")
		is_up = Input.is_action_pressed("up{0}".format([player_id + 1]), true) or touch_screen_result("up")
		is_back = Input.is_action_just_pressed("back{0}".format([player_id + 1]), true) or touch_screen_result("back")
		is_accept = Input.is_action_just_pressed("accept{0}".format([player_id + 1]), true) or touch_screen_result("accept")
		is_slow = touch_screen_result("slow")
	
	var reversed := is_cursed and (!ai_level or randf() < 0.375)
	var velocity_to_add : Vector2 = Vector2(0, VELOCITY_BOOST * delta * speed_modifier) * (-1 if reversed else 1)
	if is_down:
		velocity += velocity_to_add
	UpParticles.emitting = is_down if !reversed else is_up
	if is_up:
		velocity -= velocity_to_add
	DownParticles.emitting = is_up if !reversed else is_down
	if is_back and !has_attached_ball():
		if second_extension_on:
			Animations.play("ExtensionChange", -1, -1, true)
		else:
			Animations.play("ExtensionChange", -1, 1)
		second_extension_on = !second_extension_on
	if has_attached_ball() and RayCast.get_collision_normal() != Vector2.ZERO:
		var ball : Ball = BallPoint.get_child(0)
		var orientation := ball.linear_velocity + RayCast.get_collision_normal() * ball_target_speed
		if Arrow.visible:
			var base_rotation := orientation.angle() - Arrow.rotation
			Arrow.rotation += [orientation.angle() - Arrow.rotation - 2*PI, orientation.angle() - Arrow.rotation + 2*PI].reduce(min_abs, base_rotation) * 0.25
		if is_accept:
			ball.linear_velocity = orientation
			var old_position := ball.global_position
			BallPoint.remove_child(ball)
			get_parent().add_child(ball)
			ball.global_position = old_position
			ball.unattached()
			Arrow.visible = false
	var collision := move_and_collide(velocity)
	if collision:
		velocity = collision.get_collider_velocity()
	velocity *= 1 - (delta * VELOCITY_DECELERATION * (3 if is_slow else 1))
	Follow.progress = clamp(Follow.progress - velocity.y / current_scale_multiplier / base_bar_scale.y, 0, 250.47)
	
	for index in touch_events.keys():
		if !touch_events[index].pressed:
			touch_events.erase(index)
			touch_durations.erase(index)

func min_abs(acc: float, value: float):
	if abs(acc) < abs(value):
		return acc
	return value

func set_color(color: Color):
	$BaseCollision.debug_color = Color(color, 0.3)
	
func attach_ball(ball: Ball):
	ball_ai = ball
	if !ball.speed_reset.is_connected(speed_reset):
		ball.speed_reset.connect(speed_reset)
	if !ball.finished_appearing.is_connected(ball_finished_appearing):
		ball.finished_appearing.connect(ball_finished_appearing)
	if second_extension_on:
		Animations.play("ExtensionChange", -1, -1, true)
	second_extension_on = false
	extension_animation_speed = 0
	BallPoint.scale = Vector2.ONE if !player_id else Vector2(-1, 1)
	if ball.get_parent():
		ball.get_parent().remove_child.call_deferred(ball)
	BallPoint.add_child.call_deferred(ball)
	ball.position = Vector2.ZERO
	ball.attached = true
	if !ball.appearing:
		ball_finished_appearing()
	
func ball_finished_appearing() -> void:
	if is_arrow_visible:
		Arrow.visible = true
		if RayCast.get_collision_normal() != Vector2.ZERO:
			Arrow.rotation = (RayCast.get_collision_normal()).angle()
		else:
			Arrow.rotation = - (scale * Vector2(1, 0)).angle()

func has_attached_ball() -> bool:
	return BallPoint.get_child_count() and !BallPoint.get_child(0).appearing
	
func get_ball_respawn_position():
	return Follow.global_position

func set_path_point_closest_to(ball: Ball):
	var curve := Path.curve
	var path_transform := Path.global_transform
	var closest_offset := curve.get_closest_offset(path_transform.affine_inverse() * ball.position)
	Follow.progress = closest_offset

func change_scale(new_scale: float):
	if AestethicAnimations.is_playing() && AestethicAnimations.current_animation == "BlinkScale":
		AestethicAnimations.play("RESET")
	if current_scale_multiplier + new_scale + handicap_size_offset < 3 and current_scale_multiplier + new_scale + handicap_size_offset > 0.3:
		apply_scale(Vector2(1, (current_scale_multiplier + new_scale)/current_scale_multiplier))
		Base.apply_scale(Vector2((current_scale_multiplier + new_scale)/current_scale_multiplier, 1))
		current_scale_multiplier += new_scale
	ScaleChangeTimer.start()
	
func _scale_change_timeout():
	AestethicAnimations.play("BlinkScale")
	
func _reset_scale():
	current_scale_multiplier = 1
	# Negative x values for scale is converted into negative y and 180° rotation. So putting it back means clearing rotation
	rotation = 0
	scale = base_bar_scale
	Base.scale = base_base_scale

func _on_first_timer_timeout():
	initial_direction_ai *= -1
	SecondTimer.start(SecondTimer.wait_time)

func _on_second_timer_timeout():
	timer_finished_ai = true

func change_speed(new_speed: float):
	if speed_modifier * new_speed < 2.5 and speed_modifier * new_speed > 0.4: 
		speed_modifier *= new_speed

func speed_reset(previous_modifier: float):
	speed_modifier /= previous_modifier

func set_beam(has_beam: bool):
	Beam.visible = has_beam
	if has_beam:
		BeamAnimations.play("BeamBlink")
	else:
		BeamAnimations.stop()

func options_update(State: StateType):
	is_arrow_visible = State.is_arrow_visible
	if has_attached_ball():
		Arrow.visible = is_arrow_visible
	set_handicap_scale(State.handicap[player_id].size)
	
func set_handicap_scale(handicap_size_level: int):
	var handicap_size := (handicap_size_level * 0.2 + 1)**2
	handicap_size_offset = handicap_size - 1
	base_bar_scale.y = handicap_size * sign(base_bar_scale.y)
	if is_node_ready():
		_reset_scale()
		
func curse(do_curse: bool):
	is_cursed = do_curse
	if is_cursed:
		if CurseAnimations.is_playing():
			CurseAnimations.advance(-CurseAnimations.current_animation_position)
		else:
			CurseAnimations.play("Curse")

func _on_state_timer_timeout() -> void:
	idle_state_ai = !idle_state_ai
	match [ai_level, idle_state_ai]:
		[1, false]:
			StateTimer.start(randf() * 0.5 + 0.5)
		[1, true]:
			StateTimer.start(randf() * 0.5 + 2)
		[2, true]:
			StateTimer.start(randf() * 0.5 + 1)
		[2, false]:
			StateTimer.start(randf() * 0.5 + 1.5)

func _on_beam_ai_timer_timeout() -> void:
	move_beam_ai = !move_beam_ai
	if move_beam_ai:
		BeamAITimer.start(randf() * 0.125 + 0.125)
	else:
		BeamAITimer.start(randf() * 0.125 + 0.25)
