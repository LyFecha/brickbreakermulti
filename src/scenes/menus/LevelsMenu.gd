extends Control

const ORDER_LABEL := ["Niveaux :\nDans l'ordre", "Niveaux :\nAléatoire", "Niveaux :\nTournoi"]
const PLAYER_LABEL := ["Joueur {0} :\nHumain", "Joueur {0} :\nIA (niv. 1)", "Joueur {0} :\nIA (niv. 2)", "Joueur {0} :\nIA (niv. 3)"]
const CUSTOM_LEVELS_LABEL := ["Non", "Oui"]
const LevelButton : PackedScene = preload("res://src/scenes/commons/LevelButton.tscn")

@export var State: StateType

@onready var Levels : GridContainer = $Panel/Scroll/Levels
@onready var Buttons : HBoxContainer = $Panel/Buttons
@onready var All : Button = $Panel/Scroll/Levels/All
@onready var Order : Button = $Panel/Buttons/Order
@onready var Player1 : Button = $Panel/Buttons/Player1
@onready var Player2 : Button = $Panel/Buttons/Player2
@onready var Play : Button = $Panel/Buttons/Play
@onready var Exit : Button = $Panel/Buttons/Exit
@onready var TournamentOptions: CenterContainer = $Panel/Scroll/TournamentOptions
@onready var NumberLevel: SpinBox = $Panel/Scroll/TournamentOptions/GridContainer/NumberLevel
@onready var CustomLevels: Button = $Panel/Scroll/TournamentOptions/GridContainer/CustomLevels

var selected_levels : Array[int] = []
var order : int = 0
var tournament_level_number := 3
var tournament_with_custom_levels := false

var Level1 : Button

func _ready():
	var lbs : Array[Button] = [Exit]
	for i in range(State.BASE_LEVEL_MAX):
		var lb := add_level(i, str(i + 1), lbs)
		lbs.append(lb)
		if i == 0:
			lb.grab_focus()
			for button: Button in Buttons.get_children():
				button.focus_neighbor_bottom = lb.get_path()
	for i in range(len(State.custom_levels)):
		var custom_level := State.custom_levels[i]
		if custom_level.validated:
			var lb := add_level(State.BASE_LEVEL_MAX + i, custom_level.name, lbs)
			lbs.append(lb)
	for i in range(-6, 0):
		lbs[i].focus_neighbor_bottom = Order.get_path()
	lbs[-6].focus_neighbor_bottom = All.get_path()
	All.focus_neighbor_top = lbs[-6].get_path()
	All.focus_neighbor_left = lbs[-1].get_path()
	All.focus_previous = lbs[-1].get_path()
	Level1 = lbs[1]
	set_focus_buttons(All.get_path(), Level1.get_path())
			
	State.ai_level = [0, 0]
	Player1.text = PLAYER_LABEL[0].format([1])
	Player2.text = PLAYER_LABEL[0].format([2])
	Order.text = ORDER_LABEL[0]
	All.move_to_front()
	All.visible = State.mode == E.Mode.MATCH
	Order.visible = State.mode == E.Mode.MATCH
	Play.visible = State.mode == E.Mode.MATCH
	NumberLevel.value = tournament_level_number
	NumberLevel.max_value = State.BASE_LEVEL_MAX
	CustomLevels.text = CUSTOM_LEVELS_LABEL[int(tournament_with_custom_levels)]
	Levels.visible = true
	TournamentOptions.visible = false

func add_level(i: int, name_: String, lbs: Array[Button]) -> Button:
	var lb : Button = LevelButton.instantiate()
	Levels.add_child(lb)
	lb.label.text = name_
	if i < State.BASE_LEVEL_MAX:
		lb.preview.texture = load("res://src/assets/previews/level{0}.png".format([i]))
		lb.tooltip_text = C.LEVEL_NAMES[i]
	else:
		var custom_level := State.custom_levels[i - State.BASE_LEVEL_MAX]
		if custom_level.preview_path:
			lb.preview.texture = _load(custom_level.preview_path)
		lb.tooltip_text = custom_level.name
		
	lb.pressed.connect(_on_level_pressed.bind(i))
	lb.toggled.connect(_on_level_toggled.bind(i))
	lbs[-1].focus_neighbor_right = lb.get_path()
	lbs[-1].focus_next = lb.get_path()
	lb.focus_neighbor_left = lbs[-1].get_path()
	lb.focus_previous = lbs[-1].get_path()
	if len(lbs) >= 6:
		lbs[-6].focus_neighbor_bottom = lb.get_path()
		lb.focus_neighbor_top = lbs[-6].get_path()
	else:
		lb.focus_neighbor_top = lbs[0].get_path()
	return lb

func _load(path: String) -> ImageTexture:
	var image = Image.new()
	var err = image.load(path)
	if(err != 0):
		print("Error loading the image at: " + path)
		return null
	return ImageTexture.create_from_image(image)

func _on_level_pressed(level: int):
	if State.mode == E.Mode.DUEL:
		State.current_level = level
		State.total_score = [0, 0]
		get_tree().change_scene_to_file("res://src/scenes/arena/Arena.tscn")

func _on_level_toggled(toggled_on: bool, level: int):
	if State.mode == E.Mode.MATCH:
		if toggled_on:
			selected_levels.append(level)
		else:
			selected_levels.erase(level)
		toggle_play_button()

func _on_all_pressed():
	var do_press := len(selected_levels) != Levels.get_child_count() - 1
	for level: Button in Levels.get_children():
		level.button_pressed = do_press
	All.button_pressed = false
		
func _on_order_pressed():
	order = (order + 1) % len(ORDER_LABEL)
	Order.text = ORDER_LABEL[order]
	TournamentOptions.visible = order == 2
	Levels.visible = order != 2
	toggle_play_button(order == 2)
	if order == 2:
		set_focus_buttons(CustomLevels.get_path(), NumberLevel.get_path())
	else:
		set_focus_buttons(All.get_path(), Level1.get_path())
	
func set_focus_buttons(focus1: NodePath, focus2: NodePath):
	Order.focus_neighbor_left = focus1
	Order.focus_neighbor_top = focus1
	Order.focus_previous = focus1
	Player2.focus_neighbor_top = focus1
	Play.focus_neighbor_top = focus1
	Exit.focus_neighbor_top = focus1
	
	Order.focus_neighbor_bottom = focus2
	Player2.focus_neighbor_bottom = focus2
	Play.focus_neighbor_bottom = focus2
	Exit.focus_next = focus2
	Exit.focus_neighbor_bottom = focus2
	Exit.focus_neighbor_right = focus2

func _on_play_pressed():
	if order == 2:
		var all_levels = range(State.BASE_LEVEL_MAX + (0 if !tournament_with_custom_levels else len(State.custom_levels)))
		all_levels.shuffle()
		selected_levels.assign(all_levels.slice(0, tournament_level_number))
	elif order == 1:
		selected_levels.shuffle()
	else:
		selected_levels.sort()
	State.level_index = 0
	State.level_order = selected_levels
	State.current_level = selected_levels[0]
	State.total_score = [0, 0]
	get_tree().change_scene_to_file("res://src/scenes/arena/Arena.tscn")

func _on_exit_pressed():
	get_tree().change_scene_to_file("res://src/scenes/menus/MainMenu.tscn")

func _on_player_pressed(player_id: int) -> void:
	var player_button : Button = [Player1, Player2][player_id]
	State.ai_level[player_id] = (State.ai_level[player_id] + 1) % 4
	player_button.text = PLAYER_LABEL[State.ai_level[player_id]].format([player_id + 1])

func _on_number_level_value_changed(value: int) -> void:
	tournament_level_number = value

func _on_custom_levels_toggled(toggled_on: bool) -> void:
	tournament_with_custom_levels = toggled_on
	CustomLevels.text = CUSTOM_LEVELS_LABEL[int(toggled_on)]

func toggle_play_button(force_enable: bool = false) -> void:
	Play.disabled = !force_enable && len(selected_levels) == 0
	
