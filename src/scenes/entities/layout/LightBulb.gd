extends TextureRect

@onready var light: TextureRect = $Light

func _ready() -> void:
	light.hide()

func goal(player_id: int) -> void:
	light.modulate = C.COLORS[player_id]
	light.show()
