extends Brick

func set_variant(variant: int):
	if variant:
		return load("res://src/scenes/entities/bricks/SlimBrick.tscn").instantiate() as Brick
