@tool
extends Brick

@export var player_id_side : int = -1

@onready var Overlay: Polygon2D = $Overlay

func change_state(new_id: int):
	if !Overlay or !Logic:
		Overlay = $Overlay
		Logic = $BrickLogic
	player_id_side = new_id
	Overlay.visible = player_id_side >= 0
	Overlay.scale.x = (-1 if player_id_side else 1) * abs($Overlay.scale.x)
	Overlay.modulate = Color("ff0000ff") if player_id_side else Color("0080ffff")
	Logic.set_collision_layer_value(3, player_id_side == -1)
	Logic.set_collision_layer_value(4, player_id_side == 1)
	Logic.set_collision_layer_value(5, player_id_side == 0)

func _on_brick_logic_effect(ball: Ball, type: E.BrickEffect):
	match type:
		E.BrickEffect.OWNER:
			change_state(ball.player_id if player_id_side < 0 else -1)
	effect.emit(ball, type)
	
func set_variant(variant: int):
	change_state(variant - 1)

func _on_toggled_visible(_is_visible: bool) -> void:
	change_state(-1)
